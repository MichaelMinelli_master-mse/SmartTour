import { FormGroup } from '@angular/forms';
import { Injectable } from '@angular/core';

@Injectable()
export class ToolboxService {

	constructor() { }

	FORM_markAllAsTouched(fb: FormGroup) {
		for (let c in fb.controls) {
			fb.controls[c].markAsTouched();

			if (fb.controls[c] instanceof FormGroup)
				this.FORM_markAllAsTouched(fb.controls[c] as FormGroup)
		}
	}

	static IMG_randomParam(): string {
		return String(Math.floor((Math.random()*10000000)))
	}
}
