import {Directive} from "@angular/core";
import {NG_VALIDATORS, Validator, FormControl} from "@angular/forms";

@Directive({
    selector: "[requireFake]"
})
export class FakeValidator implements Validator {
    static validate(c: FormControl): {[key: string]: any} {
      return null;
    }

    validate(c: FormControl): {[key: string]: any} {
      return FakeValidator.validate(c);
    }
}
