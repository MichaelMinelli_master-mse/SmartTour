/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { POIsManagerService } from './pois-manager.service';

describe('Service: Login', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [POIsManagerService]
    });
  });

  it('should ...', inject([POIsManagerService], (service: POIsManagerService) => {
    expect(service).toBeTruthy();
  }));
});
