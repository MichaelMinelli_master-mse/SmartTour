import { Place } from './../Place';
import { PointOfInterest } from './../PointOfInterest';
import { SessionManagerService } from './../session-manager/session-manager.service';
import { Router } from '@angular/router';
import { SettingsService } from './../../core/settings/settings.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { BackendProviderService, BackendResponse } from './../backend-provider/backend-provider.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { User } from '../User';

@Injectable()
export class POIsManagerService {

	allPOIs: Array<PointOfInterest> = []

	constructor(private http: HttpClient, private backend: BackendProviderService, private sessionManager: SessionManagerService) {	}

	getAll(place: Place): Observable<[PointOfInterest]> {
		return Observable.create(observer => {
			this.backend.execute(this.http.get<BackendResponse>(this.backend.LIST_POIs.replace("{place}", String(place.id)))).subscribe(
				data => {
					if (data.result) {
						observer.next(data.data.POIs.map(poi => PointOfInterest.fromJSON(poi)))
					} else {
						observer.error(data)
					}
					observer.complete()
				}
			)
		});
	}

	add(poi: PointOfInterest): Observable<BackendResponse> {
		return Observable.create(observer => {
			let fd = new FormData();
			fd.append('name', poi.name);
			fd.append('order', String(poi.order));
			fd.append('latitude', String(poi.latitude));
			fd.append('longitude', String(poi.longitude));
			fd.append('place', String(poi.placeID));

			this.backend.execute(this.http.post<BackendResponse>(this.backend.ADD_POI.replace("{place}", String(poi.placeID)), fd)).subscribe(
				data => {
					observer.next(data)
					observer.complete()
				}
			)
		});
	}

	modify(poi: PointOfInterest): Observable<BackendResponse> {
		return Observable.create(observer => {
			let fd = new FormData();
			fd.append('name', poi.name);
			fd.append('order', String(poi.order));
			fd.append('latitude', String(poi.latitude));
			fd.append('longitude', String(poi.longitude));
			fd.append('place', String(poi.placeID));

			this.backend.execute(this.http.put<BackendResponse>(this.backend.MODIFY_POI.replace("{place}", String(poi.placeID)).replace("{id}", String(poi.id)), fd)).subscribe(
				data => {
					observer.next(data)
					observer.complete()
				}
			)
		});
	}

	delete(poi: PointOfInterest): Observable<BackendResponse> {
		return Observable.create(observer => {
			this.backend.execute(this.http.delete<BackendResponse>(this.backend.DELETE_POI.replace("{place}", String(poi.placeID)).replace("{id}", String(poi.id)))).subscribe(
				data => {
					observer.next(data)
					observer.complete()
				}
			)
		});
	}
}
