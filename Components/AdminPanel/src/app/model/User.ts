import { Place } from './Place';

export class User {
	constructor();
	constructor(mail: string, firstName: string, lastName: string, placeAdmin: boolean);
	constructor(mail: string, firstName: string, lastName: string, placeAdmin: boolean, password: string);
	constructor(mail: string, firstName: string, lastName: string, placeAdmin: boolean, password: string, places: Array<Place>);
	constructor(mail: string);
	constructor(public mail="", public firstName="", public lastName="", public placeAdmin=false, public password="", public places=Array<Place>()) { }

	static fromJSON(user: any): User {
		let places = user.places == undefined ? [] : user.places.map(place => Place.fromJSON(place))
		return new User(user.mail, user.firstName, user.lastName, user.placeAdmin, user.password, places)
	}
}
