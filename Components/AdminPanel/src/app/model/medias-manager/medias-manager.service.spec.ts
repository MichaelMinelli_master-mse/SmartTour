/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MediasManagerService } from './medias-manager.service';

describe('Service: Login', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MediasManagerService]
    });
  });

  it('should ...', inject([MediasManagerService], (service: MediasManagerService) => {
    expect(service).toBeTruthy();
  }));
});
