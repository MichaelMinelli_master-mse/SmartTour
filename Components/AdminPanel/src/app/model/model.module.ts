import { POIsManagerService } from './pois-manager/pois-manager.service';
import { AccessManagerService } from './access-manager/access-manager.service';
import { PlacesManagerService } from './places-manager/places-manager.service';
import { UsersManagerService } from './users-manager/users-manager.service';
import { User } from './User';
import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { SessionManagerService } from './session-manager/session-manager.service';
import { BackendProviderService } from './backend-provider/backend-provider.service';
import { MediasManagerService } from './medias-manager/medias-manager.service';

@NgModule({
    imports: [
		BrowserModule,
    	HttpClientModule
    ],
    providers: [
		SessionManagerService,
		BackendProviderService,
		UsersManagerService,
		PlacesManagerService,
		AccessManagerService,
		POIsManagerService,
		MediasManagerService
    ],
    declarations: [
	],
    exports: [
    ]
})

export class ModelModule {
    constructor() {

    }
}
