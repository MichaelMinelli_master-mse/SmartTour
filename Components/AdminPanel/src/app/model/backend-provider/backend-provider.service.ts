import { AuthService } from './../../auth/auth.service';
import { BackendResponse } from './backend-provider.service';
import { Globals } from './../globals/globals.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

// https://medium.com/codingthesmartway-com-blog/angular-4-3-httpclient-accessing-rest-web-services-with-angular-2305b8fd654b

export interface BackendResponse {
	result: boolean;
	code: number;
	description: string;
	timestamp: string;
	data: any;
	sessionToken: string;
}

enum GlobalErrorCode {
	UNKNOWN = -1000,
	NO_SESSION = -1001,
	NOT_LOGIN = -1002,
	INSUFFICIENT_PERMISSION = -1003
}

@Injectable()
export class BackendProviderService {

	public static BACKEND_BASE_URL: string = Globals.DEPLOY ? "http://api.smarttour.michaelminelli.ch/" : "http://localhost:8181/"
	public static BACKEND_V1: string = BackendProviderService.BACKEND_BASE_URL + "v1/"

	//User Session
	public LOGIN_URL: string 		= BackendProviderService.BACKEND_V1 + "login/{mail}/{password}"
	public LOGOUT_URL: string 		= BackendProviderService.BACKEND_V1 + "logout"
	public RESTORE_SESSION: string 	= BackendProviderService.BACKEND_V1 + "restore_session"

	//Users
	public LIST_USERS: string 		= BackendProviderService.BACKEND_V1 + "users"
	public ADD_USER: string 		= BackendProviderService.BACKEND_V1 + "users"
	public MODIFY_USER: string 		= BackendProviderService.BACKEND_V1 + "users/{mail}"
	public MODIFY_PASSWORD: string 	= BackendProviderService.BACKEND_V1 + "users/{mail}/password"
	public DELETE_USER: string 		= BackendProviderService.BACKEND_V1 + "users/{mail}"

	//Places
	public GET_PLACE: string 		= BackendProviderService.BACKEND_V1 + "places/{id}"
	public LIST_PLACES: string 		= BackendProviderService.BACKEND_V1 + "places"
	public ADD_PLACES: string 		= BackendProviderService.BACKEND_V1 + "places"
	public MODIFY_PLACES: string 	= BackendProviderService.BACKEND_V1 + "places/{id}"
	public DELETE_PLACE: string 	= BackendProviderService.BACKEND_V1 + "places/{id}"

	//Access
	public ADD_ACCESS: string 		= BackendProviderService.BACKEND_V1 + "places/{id}/add-user"
	public DELETE_ACCESS: string 	= BackendProviderService.BACKEND_V1 + "places/{id}/remove-user/{mail}"

	//PointOfInterest
	public LIST_POIs: string 		= BackendProviderService.BACKEND_V1 + "places/{place}/POIs"
	public ADD_POI: string 			= BackendProviderService.BACKEND_V1 + "places/{place}/POIs"
	public MODIFY_POI: string 		= BackendProviderService.BACKEND_V1 + "places/{place}/POIs/{id}"
	public DELETE_POI: string 		= BackendProviderService.BACKEND_V1 + "places/{place}/POIs/{id}"

	//PointOfInterest
	public LIST_MEDIAS: string 		= BackendProviderService.BACKEND_V1 + "medias"
	public ADD_MEDIA: string 		= BackendProviderService.BACKEND_V1 + "medias"
	public MODIFY_MEDIA: string 	= BackendProviderService.BACKEND_V1 + "medias/{id}"
	public DELETE_MEDIA: string 	= BackendProviderService.BACKEND_V1 + "medias/{id}"

  	constructor(private http: HttpClient, private auth: AuthService, private router: Router) { }

	execute(request: Observable<BackendResponse>, withRedirection = true): Observable<BackendResponse> {
		return Observable.create(observer => {
			request.subscribe(
				data => {
					this.auth.setToken(data.sessionToken)

					if (withRedirection && !data.result) {
						switch (data.code) {
							case GlobalErrorCode.NO_SESSION.valueOf():
							case GlobalErrorCode.NOT_LOGIN.valueOf():
								this.router.navigate(["/login"])
								break;

							case GlobalErrorCode.INSUFFICIENT_PERMISSION.valueOf():
								this.router.navigate(["/home"])
								break;

							default:
								break;
						}
					}

					observer.next(data)
					observer.complete()
				},
				err => {
					observer.next({ result: false, code: -2000, description: "", timestamp: "", data: null, sessionToken: "" })
					observer.complete()
				}
			)
		});
	}
}
