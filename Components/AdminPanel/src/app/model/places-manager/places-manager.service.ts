import { Place } from './../Place';
import { SessionManagerService } from './../session-manager/session-manager.service';
import { Router } from '@angular/router';
import { SettingsService } from './../../core/settings/settings.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { BackendProviderService, BackendResponse } from './../backend-provider/backend-provider.service';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable()
export class PlacesManagerService {

	allPlaces: Array<Place> = []

	constructor(private http: HttpClient, private backend: BackendProviderService, private sessionManager: SessionManagerService) {	}

	getAll(): Observable<[Place]> {
		return Observable.create(observer => {
			this.backend.execute(this.http.get<BackendResponse>(this.backend.LIST_PLACES + "?lightVersion=false")).subscribe(
				data => {
					if (data.result) {
						observer.next(data.data.places.map(place => Place.fromJSON(place)))
					} else {
						observer.error(data)
					}
					observer.complete()
				}
			)
		});
	}

	get(placeID: number): Observable<Place> {
		return Observable.create(observer => {
			this.backend.execute(this.http.get<BackendResponse>(this.backend.GET_PLACE.replace("{id}", String(placeID)) + "?lightVersion=true")).subscribe(
				data => {
					if (data.result) {
						observer.next(Place.fromJSON(data.data.place))
					} else {
						observer.error(data)
					}
					observer.complete()
				}
			)
		});
	}

	add(place: Place): Observable<BackendResponse> {
		place.refreshImgRandomParam()
		return Observable.create(observer => {
			let fd = new FormData();
			fd.append('name', place.name);
			fd.append('description', place.description);
			fd.append('picture', place.picture);

			this.backend.execute(this.http.post<BackendResponse>(this.backend.ADD_PLACES, fd)).subscribe(
				data => {
					observer.next(data)
					observer.complete()
				}
			)
		});
	}

	modify(place: Place): Observable<BackendResponse> {
		place.refreshImgRandomParam()
		return Observable.create(observer => {
			let fd = new FormData();
			fd.append('name', place.name);
			fd.append('description', place.description);
			fd.append('picture', place.picture);

			this.backend.execute(this.http.put<BackendResponse>(this.backend.MODIFY_PLACES.replace("{id}", place.id.toString()), fd)).subscribe(
				data => {
					observer.next(data)
					observer.complete()
				}
			)
		});
	}

	delete(place: Place): Observable<BackendResponse> {
		return Observable.create(observer => {
			this.backend.execute(this.http.delete<BackendResponse>(this.backend.DELETE_PLACE.replace("{id}", place.id.toString()))).subscribe(
				data => {
					observer.next(data)
					observer.complete()
				}
			)
		});
	}
}
