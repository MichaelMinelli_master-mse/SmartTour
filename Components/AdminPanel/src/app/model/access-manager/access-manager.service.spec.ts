import { TestBed, inject } from '@angular/core/testing';

import { AccessManagerService } from './access-manager.service';

describe('AccessManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccessManagerService]
    });
  });

  it('should be created', inject([AccessManagerService], (service: AccessManagerService) => {
    expect(service).toBeTruthy();
  }));
});
