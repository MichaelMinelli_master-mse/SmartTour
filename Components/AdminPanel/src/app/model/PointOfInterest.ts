import { ToolboxService } from './../core/toolbox/toolbox.service';
import { BackendProviderService } from './backend-provider/backend-provider.service';
import { User } from './User';
import { Place } from './Place';

export class PointOfInterest {
	//public FILES_PLACES_IMAGES: string = BackendProviderService.BACKEND_BASE_URL + "places/images/"

	constructor();
	constructor(id: number);
	constructor(id: number, name: string, order: number, latitude: number, longitude: number, placeID: number, place: Place);
	constructor(public id=-1, public name="", public order=0, public latitude=0.0, public longitude=0.0, public placeID=-1, public place=new Place()) { }

	static fromJSON(pot: any): PointOfInterest {
		let place = pot.place == undefined ? new Place() : Place.fromJSON(pot.place)
		return new PointOfInterest(pot.id, pot.name, pot.order, pot.latitude, pot.longitude, pot.placeID, place)
	}
}
