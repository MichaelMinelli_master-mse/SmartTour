import { Place } from './Place';
import { User } from "./User";

export class Access {
	constructor();
	constructor(user: User, place: Place);
	constructor(public user=new User(), public place=new Place()) { }

	static fromJSON(access: any): Access {
		return new Access(User.fromJSON(access.user), Place.fromJSON(access.place))
	}
}
