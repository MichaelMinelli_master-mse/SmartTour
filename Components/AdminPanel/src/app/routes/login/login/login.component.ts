import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { BackendResponse } from './../../../model/backend-provider/backend-provider.service';
import { SessionManagerService } from './../../../model/session-manager/session-manager.service';
import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../../core/settings/settings.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

	public loading: boolean = false

	valForm: FormGroup;

	loginError: string = "";

    constructor(public settings: SettingsService, fb: FormBuilder, private sessionManager: SessionManagerService, private router: Router) {
		sessionManager.verifySession(false).subscribe(
			data => {
				if (data)
					this.router.navigate(["/home"])
			}
		)

        this.valForm = fb.group({
            'email': [null, Validators.compose([Validators.required, CustomValidators.email])],
            'password': [null, Validators.required]
		});
    }

    submitForm($ev, value: any) {
		$ev.preventDefault();

        for (let c in this.valForm.controls) {
            this.valForm.controls[c].markAsTouched();
		}

        if (this.valForm.valid) {
			this.loading = true
			this.sessionManager.tryToLogin(value.email, value.password).subscribe(
				data => {
					this.router.navigate(["/home"])
					this.loading = false
				},
				err => {
					console.log(err)
					this.loginError = (err as BackendResponse).description
					this.loading = false
				}
			)
        }
	}

    ngOnInit() { }

}
