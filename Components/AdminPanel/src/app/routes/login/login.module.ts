import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { SharedComponentsModule } from '../../sharedComponents/sharedComponents.module';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
    { path: '', component: LoginComponent },
];

@NgModule({
    imports: [
	  SharedModule,
	  SharedComponentsModule
    ],
    declarations: [LoginComponent],
    exports: [
        RouterModule
    ]
})
export class LoginModule { }
