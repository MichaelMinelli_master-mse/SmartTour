import { LayoutComponent } from '../layout/layout.component';
import { LoginComponent } from './login/login/login.component';

export const routes = [

    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', loadChildren: './home/home.module#HomeModule' },
			{ path: 'admin', loadChildren: './admin/admin.module#AdminModule' },
			{ path: 'place', loadChildren: './place/place.module#PlaceModule' }
        ]
    },

    // Not lazy-loaded routes
    { path: 'login', component: LoginComponent },

    // Not found
    { path: '**', redirectTo: 'home' }

];
