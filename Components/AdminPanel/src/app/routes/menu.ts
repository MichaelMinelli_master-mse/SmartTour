
const Home = {
    text: 'mainTitle',
    link: '/home',
    icon: 'icon-home',
    placeAdmin: false,
    translate: 'menu.nav.HOME'
};

const ManagerUsers = {
    text: 'mainTitle',
    link: '/admin/users/list',
    icon: 'icon-people',
    placeAdmin: true,
    translate: 'menu.nav.MANAGE_USERS'
};

const ManagePlaces = {
    text: 'mainTitle',
    link: '/admin/places/list',
    icon: 'icon-settings',
    placeAdmin: true,
    translate: 'menu.nav.MANAGE_PLACES'
};

const headingMain = {
    text: 'home',
    heading: true,
    placeAdmin: false,
    translate: 'menu.heading.MAIN'
};

const headingAdmin = {
    text: 'admin',
    heading: true,
    placeAdmin: true,
    translate: 'menu.heading.ADMIN'
};

const headingPlaces = {
    text: 'places',
    heading: true,
    placeAdmin: false,
    translate: 'menu.heading.PLACES'
};

export const menu = [
    headingMain,
	Home,
	headingAdmin,
	ManagerUsers,
	ManagePlaces,
	headingPlaces
];
