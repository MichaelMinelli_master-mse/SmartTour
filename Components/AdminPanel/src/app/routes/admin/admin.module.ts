import { SharedModule } from './../../shared/shared.module';
import { PlacesComponent } from './places/places/places.component';
import { AddUserComponent } from './users/add-user/add-user.component';
import { ModifyPasswordComponent } from './users/modify-password/modify-password.component';
import { CustomFormsModule } from 'ng2-validation';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users/users/users.component';
import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModifyUserComponent } from './users/modify-user/modify-user.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PlacesUsersComponent } from './places/places-users/places-users.component';
import { AddAccessDialog } from './places/places-users/add-access-dialog/add-access-dialog.component';
import { AddModifyPlaceComponent } from './places/add-modify-place/add-modify-place.component';

const routes: Routes = [
	//Users
    { path: 'users/list', 			component: UsersComponent },
    { path: 'users/modify/:mail', 	component: ModifyUserComponent },
    { path: 'users/password/:mail', component: ModifyPasswordComponent },
	{ path: 'users/add', 			component: AddUserComponent },

	//Places
    { path: 'places/list', 			component: PlacesComponent },
    { path: 'places/add', 			component: AddModifyPlaceComponent },
    { path: 'places/modify/:id', 	component: AddModifyPlaceComponent },
    { path: 'places/:id/users', 	component: PlacesUsersComponent }
];

@NgModule({
    imports: [
		SharedModule,
		RouterModule.forChild(routes),
		TranslateModule,
		CommonModule,
		CustomFormsModule,
		FormsModule,
		ReactiveFormsModule
    ],
    declarations: [
		UsersComponent,
		ModifyUserComponent,
		ModifyPasswordComponent,
		AddUserComponent,
		PlacesComponent,
		PlacesUsersComponent,
		AddAccessDialog,
		AddModifyPlaceComponent
	],
  	entryComponents: [
		AddAccessDialog
	],
  	exports: [
		AddAccessDialog,
		AddModifyPlaceComponent,
        RouterModule
    ]
})
export class AdminModule { }
