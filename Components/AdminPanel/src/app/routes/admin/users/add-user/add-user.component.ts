import { ToolboxService } from './../../../../core/toolbox/toolbox.service';
import { TranslateService } from '@ngx-translate/core';
import { SessionManagerService } from './../../../../model/session-manager/session-manager.service';
import { User } from './../../../../model/User';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersManagerService } from '../../../../model/users-manager/users-manager.service';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

const swal = require('sweetalert');

//https://angular-2-training-book.rangle.io/handout/routing/routeparams.html

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

	public loading: boolean = false

	public valForm: FormGroup;

	constructor(private route: ActivatedRoute, private router: Router, private sessionManager: SessionManagerService, private usersManager: UsersManagerService, private fb: FormBuilder, private translate: TranslateService, private toolbox: ToolboxService) {
		sessionManager.verifySession(true).subscribe(data => {
			if (!(data && this.sessionManager.getUser().placeAdmin)) {
				this.router.navigate(["/home"])
			}
		})

		let password = new FormControl('', Validators.required);
		let certainPassword = new FormControl('', CustomValidators.equalTo(password));

		this.valForm = fb.group({
			'firstName': ["", Validators.required],
			'lastName': ["", Validators.required],
			'email': ["", [CustomValidators.email, Validators.required]],
			'admin': [false],
			'passwordGroup': fb.group({
				password: password,
				confirmPassword: certainPassword
			})
		});
	}

	ngOnInit() { }

	submitForm($ev, value: any) {
        $ev.preventDefault();
		this.toolbox.FORM_markAllAsTouched(this.valForm)

        if (this.valForm.valid) {
			this.loading = true
            this.usersManager.add(new User(value.email, value.firstName, value.lastName, value.admin, value.passwordGroup.password)).subscribe( data => {
				if (!data.result) {
					if (data.code == -3) {
						swal({
							title: this.translate.instant("add-user.error_mail.TITLE"),
							text: this.translate.instant("add-user.error_mail.TEXT"),
							icon: "error"
						})
					}
				} else {
					this.router.navigate(["/admin/users/list"])
				}

				this.loading = false
			})
        }
    }
}

