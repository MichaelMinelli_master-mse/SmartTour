import { Router, NavigationExtras } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { User } from './../../../../model/User';

import { SessionManagerService } from '.././../../../model/session-manager/session-manager.service';
import { Component, OnInit } from '@angular/core';
import { UsersManagerService } from '../../../../model/users-manager/users-manager.service';

const swal = require('sweetalert');


@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

	private allUsers: Array<User> = []
	public users: Array<User> = []

	public searchText: string = ""
	public loaded: boolean = false

    constructor(private sessionManager: SessionManagerService, private usersManager: UsersManagerService, public translate: TranslateService, private router: Router) {
		sessionManager.verifySession(true).subscribe(data => {
			if (!(data && this.sessionManager.getUser().placeAdmin)) {
				this.router.navigate(["/home"])
			}
		})

		this.updateUsers()
	}

	updateUsers() {
		this.usersManager.getAll().subscribe(
			users => {
				this.allUsers = this.users = users
				this.loaded = true
			}
		)
	}

	search(searchText: string) {
		if (searchText != null)
			this.searchText = searchText.toLowerCase()

		this.users = this.allUsers.filter( value => {
			return this.searchText == "" ? true : (value.firstName.toLowerCase().includes(this.searchText) || value.lastName.toLowerCase().includes(this.searchText) || value.mail.toLowerCase().includes(this.searchText))
		})
	}

	add() {
		this.router.navigate(["/admin/users/add/"])
	}

	modify(user: User) {
		this.usersManager.allUsers = this.allUsers
		this.router.navigate(["/admin/users/modify/", user.mail])
	}

	changePassword(user: User) {
		this.usersManager.allUsers = this.allUsers
		this.router.navigate(["/admin/users/password/", user.mail])
	}

	delete(user: User) {
		swal({
			title: this.translate.instant("modal.delete.TITLE"),
			text: this.translate.instant("modal.delete.TEXT"),
			confirmButtonText: this.translate.instant("modal.delete.CONFIRM"),
            cancelButtonText: this.translate.instant("modal.delete.CANCEL"),
			icon: "warning",
			buttons: true,
			dangerMode: true,
		}).then((willDelete) => {
			if (willDelete) {
				this.usersManager.delete(user).subscribe(data => {
					if (data.result) {
						let filter = value => { return value.mail != user.mail }
						this.allUsers = this.allUsers.filter(filter)
						this.search(null)

						swal(this.translate.instant("modal.delete.CONFIRMATION"), {
							icon: "success",
						});
					}
				})
			}
		})
	}

    ngOnInit() {
    }
}
