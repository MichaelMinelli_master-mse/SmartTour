import { ToolboxService } from './../../../../core/toolbox/toolbox.service';
import { TranslateService } from '@ngx-translate/core';
import { SessionManagerService } from './../../../../model/session-manager/session-manager.service';
import { User } from './../../../../model/User';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersManagerService } from '../../../../model/users-manager/users-manager.service';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

const swal = require('sweetalert');

//https://angular-2-training-book.rangle.io/handout/routing/routeparams.html

@Component({
  selector: 'app-modify-user',
  templateUrl: './modify-user.component.html',
  styleUrls: ['./modify-user.component.scss']
})
export class ModifyUserComponent implements OnInit {

	public loading: boolean = false

	private user: User

	public valForm: FormGroup;

	constructor(private route: ActivatedRoute, private router: Router, public sessionManager: SessionManagerService, private usersManager: UsersManagerService, private fb: FormBuilder, private translate: TranslateService, private toolbox: ToolboxService) {
		this.route.params.subscribe(params => {
			this.user = this.usersManager.allUsers.find(element => { return element.mail == params["mail"] });

			if (this.user == null || this.user == undefined) {
				this.router.navigate(["/admin/users/list"])
			} else {
				sessionManager.verifySession(true).subscribe(data => {
					if (!(data && (this.sessionManager.getUser().placeAdmin || this.sessionManager.getUser().mail == this.user.mail))) {
						this.router.navigate(["/home"])
					}
				})

				this.valForm = fb.group({
					'firstName': [this.user.firstName, Validators.required],
					'lastName': [this.user.lastName, Validators.required],
					'email': [this.user.mail, [CustomValidators.email, Validators.required]],
					'admin': [this.user.placeAdmin]
				});
			}
		});
	}

	ngOnInit() { }

	submitForm($ev, value: any) {
        $ev.preventDefault();
        this.toolbox.FORM_markAllAsTouched(this.valForm)

        if (this.valForm.valid) {
			this.loading = true
            this.usersManager.modify(new User(value.email, value.firstName, value.lastName, value.admin), this.user.mail).subscribe( data => {
				if (!data.result) {
					if (data.code == -3) {
						swal({
							title: this.translate.instant("modify-user.error_mail.TITLE"),
							text: this.translate.instant("modify-user.error_mail.TEXT"),
							icon: "error"
						})
					}
				} else {
					this.router.navigate(["/admin/users/list"])
				}

				this.loading = false
			})
        }
    }
}

