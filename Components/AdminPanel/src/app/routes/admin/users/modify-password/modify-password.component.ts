import { ToolboxService } from './../../../../core/toolbox/toolbox.service';
import { TranslateService } from '@ngx-translate/core';
import { SessionManagerService } from './../../../../model/session-manager/session-manager.service';
import { User } from './../../../../model/User';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersManagerService } from '../../../../model/users-manager/users-manager.service';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

const swal = require('sweetalert');

//https://angular-2-training-book.rangle.io/handout/routing/routeparams.html

@Component({
  selector: 'app-modify-password',
  templateUrl: './modify-password.component.html',
  styleUrls: ['./modify-password.component.scss']
})
export class ModifyPasswordComponent implements OnInit {

	public loading: boolean = false

	private user: User

	public valForm: FormGroup;

	constructor(private route: ActivatedRoute, private router: Router, private sessionManager: SessionManagerService, private usersManager: UsersManagerService, private fb: FormBuilder, private translate: TranslateService, private toolbox: ToolboxService) {
		this.route.params.subscribe(params => {
			this.user = this.usersManager.allUsers.find(element => { return element.mail == params["mail"] });

			if (this.user == null || this.user == undefined) {
				this.router.navigate(["/admin/users/list"])
			} else {
				sessionManager.verifySession(true).subscribe(data => {
					if (!(data && (this.sessionManager.getUser().placeAdmin || this.sessionManager.getUser().mail == this.user.mail))) {
						this.router.navigate(["/home"])
					}
				})
			}
		});

		let password = new FormControl('', Validators.required);
		let certainPassword = new FormControl('', CustomValidators.equalTo(password));

		this.valForm = fb.group({
			'passwordGroup': fb.group({
				password: password,
				confirmPassword: certainPassword
			})
		});
	}

	ngOnInit() { }

	submitForm($ev, value: any) {
        $ev.preventDefault();
        this.toolbox.FORM_markAllAsTouched(this.valForm)

        if (this.valForm.valid) {
			this.loading = true

			let user = new User(this.user.mail)
			user.password = value.passwordGroup.password
            this.usersManager.password(user).subscribe( data => {
				if (data.result) {
					this.router.navigate(["/admin/users/list"])
				}

				this.loading = false
			})
        }
    }
}
