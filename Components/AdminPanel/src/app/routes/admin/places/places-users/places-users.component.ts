import { AddAccessDialog } from './add-access-dialog/add-access-dialog.component';
import { PlacesManagerService } from './../../../../model/places-manager/places-manager.service';
import { Access } from './../../../../model/Access';
import { AccessManagerService } from './../../../../model/access-manager/access-manager.service';
import { BackendProviderService } from './../../../../model/backend-provider/backend-provider.service';
import { Place } from './../../../../model/Place';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { User } from './../../../../model/User';

import { SessionManagerService } from '.././../../../model/session-manager/session-manager.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

const swal = require('sweetalert');


@Component({
	selector: 'app-places-users',
	templateUrl: './places-users.component.html',
	styleUrls: ['./places-users.component.scss']
})
export class PlacesUsersComponent implements OnInit {

	private allUsers: Array<User> = []
	public users: Array<User> = []

	public place: Place = new Place()

	public searchText: string = ""
	public loaded: boolean = false

	constructor(private route: ActivatedRoute, public sessionManager: SessionManagerService, public accessManager: AccessManagerService, public placesManager: PlacesManagerService, private backend: BackendProviderService, public translate: TranslateService, public router: Router, public dialog: MatDialog) {
		this.route.params.subscribe(params => {
			this.place = this.placesManager.allPlaces.find(element => { return element.id == params["id"] });

			if (this.place == null || this.place == undefined || this.place.id == -1) {
				this.router.navigate(["/admin/users/list"])
			} else {
				sessionManager.verifySession(true).subscribe(data => {
					if (!(data && this.sessionManager.getUser().placeAdmin)) {
						this.router.navigate(["/home"])
					}
				})
			}
		});

		this.updateUsers()
	}

	updateUsers() {
		this.allUsers = this.users = this.place.users
		this.loaded = true
	}

	search(searchText: string) {
		if (searchText != null)
			this.searchText = searchText.toLowerCase()

		this.users = this.allUsers.filter(value => {
			return this.searchText == "" ? true : (value.firstName.toLowerCase().includes(this.searchText) || value.lastName.toLowerCase().includes(this.searchText) || value.mail.toLowerCase().includes(this.searchText))
		})
	}

	add() {
		let dialogRef = this.dialog.open(AddAccessDialog, {
			width: '250px',
			data: { users: this.allUsers }
		});

		dialogRef.afterClosed().subscribe(user => {
			if (user) {
				this.accessManager.add(new Access(user, this.place)).subscribe(
					response => {
						if (response.result) {
							this.allUsers.push(user)
							this.search(null)
						}

					}
				)
			}
		});
	}

	delete(user: User) {
		swal({
			title: this.translate.instant("modal.delete.TITLE"),
			text: this.translate.instant("modal.delete.TEXT"),
			confirmButtonText: this.translate.instant("modal.delete.CONFIRM"),
			cancelButtonText: this.translate.instant("modal.delete.CANCEL"),
			icon: "warning",
			buttons: true,
			dangerMode: true,
		}).then((willDelete) => {
			if (willDelete) {
				this.accessManager.delete(new Access(user, this.place)).subscribe(data => {
					if (data.result) {
						let filter = value => { return value.mail != user.mail }
						this.allUsers = this.allUsers.filter(filter)
						this.search(null)

						swal(this.translate.instant("modal.delete.CONFIRMATION"), {
							icon: "success",
						});
					}
				})
			}
		})
	}

	ngOnInit() {
	}
}
