import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAccessDialog } from './add-access-dialog.component';

describe('AddAccessDialogComponent', () => {
  let component: AddAccessDialog;
  let fixture: ComponentFixture<AddAccessDialog>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAccessDialog ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAccessDialog);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
