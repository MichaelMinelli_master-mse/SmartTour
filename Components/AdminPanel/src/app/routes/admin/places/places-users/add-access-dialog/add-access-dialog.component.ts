import { UsersManagerService } from './../../../../../model/users-manager/users-manager.service';
import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { User } from '../../../../../model/User';

@Component({
	selector: 'app-add-access-dialog',
	templateUrl: './add-access-dialog.component.html',
	styleUrls: ['./add-access-dialog.component.scss']
})
export class AddAccessDialog {

	public loadingStatus: string = "places-users.add-user-dialog.LOADING"
	public users: Array<User> = []

	constructor(public dialogRef: MatDialogRef<AddAccessDialog>, @Inject(MAT_DIALOG_DATA) public data: any, public usersManager: UsersManagerService) {
		usersManager.getAll().subscribe(
			users => {
				let currentUsers = (data.users as [User]).map( user => { return user.mail })
				this.users = []
				users.forEach(
					user => {
						if (!currentUsers.includes(user.mail))
							this.users.push(user)
					}
				)
				this.loadingStatus = "places-users.add-user-dialog.USERS"
			}
		)
	}

	onNoClick(): void {
		this.dialogRef.close();
	}

	getSelectedUser(): User {
		return this.users.find(value => { return value.mail == this.data.mail })
	}
}
