import { BackendProviderService } from './../../../../model/backend-provider/backend-provider.service';
import { Place } from './../../../../model/Place';
import { Router, NavigationExtras } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { User } from './../../../../model/User';

import { SessionManagerService } from '.././../../../model/session-manager/session-manager.service';
import { Component, OnInit } from '@angular/core';
import { PlacesManagerService } from '../../../../model/places-manager/places-manager.service';
import { ToolboxService } from '../../../../core/toolbox/toolbox.service';

const swal = require('sweetalert');


@Component({
    selector: 'app-places',
    templateUrl: './places.component.html',
    styleUrls: ['./places.component.scss']
})
export class PlacesComponent implements OnInit {

	private allPlaces: Array<Place> = []
	public places: Array<Place> = []

	public searchText: string = ""
	public loaded: boolean = false

    constructor(public sessionManager: SessionManagerService, public placesManager: PlacesManagerService, public backend: BackendProviderService, public translate: TranslateService, public router: Router, public toolbox: ToolboxService) {
		sessionManager.verifySession(true).subscribe(data => {
			if (!(data && this.sessionManager.getUser().placeAdmin)) {
				this.router.navigate(["/home"])
			}
		})

		this.updatePlaces()
	}

	updatePlaces() {
		this.placesManager.getAll().subscribe(
			places => {
				this.allPlaces = this.places = places
				this.loaded = true
			}
		)
	}

	search(searchText: string) {
		if (searchText != null)
			this.searchText = searchText.toLowerCase()

		this.places = this.allPlaces.filter( value => {
			return this.searchText == "" ? true : (value.name.toLowerCase().includes(this.searchText) || value.description.toLowerCase().includes(this.searchText))
		})
	}

	add() {
		this.router.navigate(["/admin/places/add/"])
	}

	modify(place: Place) {
		this.placesManager.allPlaces = this.allPlaces
		this.router.navigate(["/admin/places/modify/", place.id])
	}

	changeUsers(place: Place) {
		this.placesManager.allPlaces = this.allPlaces
		this.router.navigate(["/admin/places/" + place.id + "/users/"])
	}

	delete(place: Place) {
		swal({
			title: this.translate.instant("modal.delete.TITLE"),
			text: this.translate.instant("modal.delete.TEXT"),
			confirmButtonText: this.translate.instant("modal.delete.CONFIRM"),
            cancelButtonText: this.translate.instant("modal.delete.CANCEL"),
			icon: "warning",
			buttons: true,
			dangerMode: true,
		}).then((willDelete) => {
			if (willDelete) {
				this.placesManager.delete(place).subscribe(data => {
					if (data.result) {
						let filter = value => { return value.id != place.id }
						this.allPlaces = this.allPlaces.filter(filter)
						let user = this.sessionManager.getUser()
						user.places = user.places.filter(filter)
						this.sessionManager.setUser(user)
						this.search(null)

						swal(this.translate.instant("modal.delete.CONFIRMATION"), {
							icon: "success",
						});
					}
				})
			}
		})
	}

    ngOnInit() {
    }
}
