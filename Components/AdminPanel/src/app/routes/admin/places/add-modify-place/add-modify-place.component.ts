import { FileValidator } from './../../../../shared/directives/file-validator/file-input.validator';
import { FakeValidator } from './../../../../shared/directives/fake-validator/fake.validator';
import { BackendResponse, BackendProviderService } from './../../../../model/backend-provider/backend-provider.service';
import { Observable } from 'rxjs';
import { PlacesManagerService } from './../../../../model/places-manager/places-manager.service';
import { ToolboxService } from './../../../../core/toolbox/toolbox.service';
import { TranslateService } from '@ngx-translate/core';
import { SessionManagerService } from './../../../../model/session-manager/session-manager.service';
import { Place } from './../../../../model/Place';
import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersManagerService } from '../../../../model/users-manager/users-manager.service';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

const swal = require('sweetalert');

@Component({
  selector: 'app-add-modify-place',
  templateUrl: './add-modify-place.component.html',
  styleUrls: ['./add-modify-place.component.scss']
})
export class AddModifyPlaceComponent implements OnInit {
	@Input() resultURL: string = "/admin/places/list";

	@ViewChild('fileInput') fileInput: ElementRef;

	public title: string = ""
	public loading: boolean = false

	private place: Place
	public isModify: boolean = false

	public valForm: FormGroup;

	constructor(private route: ActivatedRoute, private router: Router, public sessionManager: SessionManagerService, private placesManager: PlacesManagerService, private fb: FormBuilder, private translate: TranslateService, private toolbox: ToolboxService, private backend: BackendProviderService) {
		this.valForm = fb.group({
			'picture': ["", FileValidator.validate],
			'name': ["", Validators.required],
			'description': ["", Validators.required]
		});

		this.route.params.subscribe(params => {
			this.place = this.placesManager.allPlaces.find(element => { return element.id == params["id"] })

			if (this.place != undefined && this.place != null) {
				sessionManager.verifySession(true).subscribe(data => {
					if (!(data && (this.sessionManager.getUser().placeAdmin || this.sessionManager.getUser().places.find(place => place.id == params["id"]) != undefined))) {
						this.router.navigate(["/home"])
					}
				})

				this.valForm = fb.group({
					'picture': ["", FakeValidator.validate],
					'name': [this.place.name, Validators.required],
					'description': [this.place.description, Validators.required]
				});

				this.isModify = true
			} else if (params["id"] != undefined && params["id"] != null) {
				this.router.navigate(["/places/list"])
			} else {
				sessionManager.verifySession(true).subscribe(data => {
					if (!(data && this.sessionManager.getUser().placeAdmin)) {
						this.router.navigate(["/home"])
					}
				})

				this.place = new Place()

				this.isModify = false
			}
		});
	}

	ngOnInit() { }

	onFileChange(event) {
		if(event.target.files.length > 0) {
			let file = event.target.files[0];
			this.valForm.get('picture').setValue(file);
		}
	}

	clearFile() {
		this.valForm.get('picture').setValue('');
		this.fileInput.nativeElement.value = '';
	}

	submitForm($ev, value: any) {
        $ev.preventDefault();
        this.toolbox.FORM_markAllAsTouched(this.valForm)

        if (this.valForm.valid) {
			this.loading = true

			this.place.name = value.name
			this.place.description = value.description
			this.place.picture = value.picture

			if (this.isModify) {
				this.placesManager.modify(this.place).subscribe( data => {
					if (data.result) {
						let user = this.sessionManager.getUser()
						user.places = user.places.map(place => {
							if (place.id == this.place.id)
								return this.place
							else
								return place
						})
						this.sessionManager.setUser(user)

						this.router.navigate([this.resultURL])
					}

					this.loading = false
				})
			} else {
				this.placesManager.add(this.place).subscribe( data => {
					if (data.result) {
						let user = this.sessionManager.getUser()
						user.places.push(this.place)
						this.sessionManager.setUser(user)

						this.router.navigate([this.resultURL])
					}

					this.loading = false
				})
			}
        }
    }
}
