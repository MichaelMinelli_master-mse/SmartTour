import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddModifyPlaceComponent } from './add-modify-place.component';

describe('AddModifyPlaceComponent', () => {
  let component: AddModifyPlaceComponent;
  let fixture: ComponentFixture<AddModifyPlaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddModifyPlaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddModifyPlaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
