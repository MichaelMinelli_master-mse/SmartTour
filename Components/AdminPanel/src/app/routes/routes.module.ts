import { HomeModule } from './home/home.module';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslatorService } from '../core/translator/translator.service';
import { MenuService } from '../core/menu/menu.service';
import { SharedModule } from '../shared/shared.module';
import { SharedComponentsModule } from '../sharedComponents/sharedComponents.module';
import { LoginModule } from './login/login.module';

import { menu } from './menu';
import { routes } from './routes';

@NgModule({
    imports: [
		SharedModule,
		SharedComponentsModule,
        RouterModule.forRoot(routes),
		LoginModule
    ],
    declarations: [

	],
    exports: [
		RouterModule
    ]
})

export class RoutesModule {
    constructor(public menuService: MenuService, tr: TranslatorService) {
        menuService.addMenu(menu);
    }
}
