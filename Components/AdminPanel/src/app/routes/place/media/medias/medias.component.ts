import { MediasManagerService } from './../../../../model/medias-manager/medias-manager.service';
import { POIsManagerService } from './../../../../model/pois-manager/pois-manager.service';
import { ToolboxService } from './../../../../core/toolbox/toolbox.service';
import { BackendProviderService } from './../../../../model/backend-provider/backend-provider.service';
import { SessionManagerService } from './../../../../model/session-manager/session-manager.service';
import { PointOfInterest } from './../../../../model/PointOfInterest';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Place } from '../../../../model/Place';
import { Media, MediaType } from '../../../../model/Media';

const swal = require('sweetalert');


@Component({
    selector: 'app-medias',
    templateUrl: './medias.component.html',
    styleUrls: ['./medias.component.scss']
})
export class MediasComponent implements OnInit {

	public poi: PointOfInterest
	public placeName: string = ""
	public poiName: string = ""

	private allMedias: Array<Media> = []
	public medias: Array<Media> = []

	public searchText: string = ""
	public loaded: boolean = false

	constructor(public sessionManager: SessionManagerService, private mediasManager: MediasManagerService, private poisManager: POIsManagerService, public backend: BackendProviderService, public translate: TranslateService, public router: Router, private route: ActivatedRoute, public toolbox: ToolboxService) {
		this.route.params.subscribe(params => {
			this.poi = this.poisManager.allPOIs.find(element => { return element.id == params["poi"] })

			if (this.poi != undefined && this.poi != null) {
				this.poiName = this.poi.name
				this.placeName = this.poi.place.name
				if (this.poiName.length > 24) {
					this.poiName = this.poiName.substr(0, 20) + "..."
				}
				if (this.placeName.length > 24) {
					this.placeName = this.placeName.substr(0, 20) + "..."
				}

				sessionManager.verifySession(true).subscribe(data => {
					this.updatePlaces()
				})
			} else if (params["place"] != undefined && params["place"] != null) {
				this.router.navigate(["/place/" + params["place"] + "/POIs"])
			}  else {
				this.router.navigate(["/home"])
			}
		});
	}

	navigate(url: string) {
		this.router.navigate([url])
	}

	updatePlaces() {
		this.loaded = false
		this.allMedias = []
		this.medias = []
		this.mediasManager.getAll(this.poi).subscribe(
			medias => {
				this.allMedias = this.medias = medias
				this.search(null)
				this.loaded = true
			}
		)
	}

	search(searchText: string) {
		if (searchText != null)
			this.searchText = searchText.toLowerCase()

		this.medias = this.allMedias.filter( value => {
			return this.searchText == "" ? true : (value.name.toLowerCase().includes(this.searchText) || value.text.toLowerCase().includes(this.searchText))
		})
	}

	private addModify(media: Media) {
	}

	add() {
		this.router.navigate(["/place/" + this.poi.place.id + "/POIs/" + this.poi.id + "/medias/add"])
	}

	modify(media: Media) {
		this.mediasManager.allMedias = this.allMedias
		this.router.navigate(["/place/" + this.poi.place.id + "/POIs/" + this.poi.id + "/medias/modify/" + media.id])
	}

	download(media: Media) {
		window.open(media.downloadURL, "_blank");
	}

	delete(media: Media) {
		swal({
			title: this.translate.instant("modal.delete.TITLE"),
			text: this.translate.instant("modal.delete.TEXT"),
			confirmButtonText: this.translate.instant("modal.delete.CONFIRM"),
            cancelButtonText: this.translate.instant("modal.delete.CANCEL"),
			icon: "warning",
			buttons: true,
			dangerMode: true,
		}).then((willDelete) => {
			if (willDelete) {
				this.mediasManager.delete(media).subscribe(data => {
					console.log(data)
					if (data.result) {
						let filter = value => { return value.id != media.id }
						this.allMedias = this.allMedias.filter(filter)
						this.search(null)

						swal(this.translate.instant("modal.delete.CONFIRMATION"), {
							icon: "success",
						});
					}
				})
			}
		})
	}

    ngOnInit() {
    }
}
