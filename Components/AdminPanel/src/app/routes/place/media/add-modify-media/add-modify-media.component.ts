import { POIsManagerService } from './../../../../model/pois-manager/pois-manager.service';
import { PointOfInterest } from './../../../../model/PointOfInterest';
import { FileValidator } from './../../../../shared/directives/file-validator/file-input.validator';
import { FakeValidator } from './../../../../shared/directives/fake-validator/fake.validator';
import { BackendResponse, BackendProviderService } from './../../../../model/backend-provider/backend-provider.service';
import { Observable } from 'rxjs';
import { PlacesManagerService } from './../../../../model/places-manager/places-manager.service';
import { ToolboxService } from './../../../../core/toolbox/toolbox.service';
import { TranslateService } from '@ngx-translate/core';
import { SessionManagerService } from './../../../../model/session-manager/session-manager.service';
import { Place } from './../../../../model/Place';
import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersManagerService } from '../../../../model/users-manager/users-manager.service';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { MediasManagerService } from '../../../../model/medias-manager/medias-manager.service';
import { Media, MediaType } from '../../../../model/Media';

const swal = require('sweetalert');

@Component({
  selector: 'app-add-modify-media',
  templateUrl: './add-modify-media.component.html',
  styleUrls: ['./add-modify-media.component.scss']
})
export class AddModifyMediaComponent implements OnInit {
	@ViewChild('fileInput') fileInput: ElementRef;

	public poi: PointOfInterest
	public placeName: string = ""
	public poiName: string = ""

	public media: Media

	public isModify: boolean = false

	public title: string = ""
	public loading: boolean = false

	public valForm: FormGroup;

	public MediaType = MediaType

	constructor(private route: ActivatedRoute, private router: Router, public sessionManager: SessionManagerService, private mediasManager: MediasManagerService, private poisManager: POIsManagerService, private fb: FormBuilder, private translate: TranslateService, private toolbox: ToolboxService, private backend: BackendProviderService) {
		this.valForm = fb.group({
			'name': ["", Validators.required],
			'type': ["", Validators.required],
			'file': [""],
			'text': [""],
			'condition': ["", Validators.required]
		});

		this.route.params.subscribe(params => {
			this.media = this.mediasManager.allMedias.find(element => { return element.id == params["id"] })
			this.poi = this.poisManager.allPOIs.find(element => { return element.id == params["poi"] })

			if (this.poi != undefined && this.poi != null) {
				this.poiName = this.poi.name
				this.placeName = this.poi.place.name
				if (this.poiName.length > 24) {
					this.poiName = this.poiName.substr(0, 20) + "..."
				}
				if (this.placeName.length > 24) {
					this.placeName = this.placeName.substr(0, 20) + "..."
				}
			}

			if (this.media != undefined && this.media != null && this.poi != undefined && this.poi != null) {
				sessionManager.verifySession(true).subscribe(data => {
					if (!(data && (this.sessionManager.getUser().placeAdmin || this.sessionManager.getUser().places.find(place => place.id == params["place"]) != undefined))) {
						this.router.navigate(["/home"])
					}
				})

				this.valForm = fb.group({
					'name': [this.media.name, Validators.required],
					'type': [this.media.type, Validators.required],
					'file': [""],
					'text': [this.media.text],
					'condition': [this.media.condition, Validators.required]
				});

				this.isModify = true
			} else if (params["id"] != undefined && params["id"] != null) {
				this.router.navigate(["/place/" + params["place"] + "/POIs/" + params["poi"] + "/medias"])
			} else if (this.poi != undefined && this.poi != null) {
				sessionManager.verifySession(true).subscribe(data => {
					if (!(data && this.sessionManager.getUser().placeAdmin)) {
						this.router.navigate(["/home"])
					}
				})

				this.media = new Media()

				this.isModify = false
			} else {
				this.router.navigate(["/place/" + params["place"] + "/POIs/" + params["poi"] + "/medias"])
			}
		});
	}

	ngOnInit() { }

	navigate(url: string) {
		this.router.navigate([url])
	}

	onFileChange(event) {
		if(event.target.files.length > 0) {
			let file = event.target.files[0];
			this.valForm.get('file').setValue(file);
		}
	}

	clearFile() {
		this.valForm.get('file').setValue('');
		this.fileInput.nativeElement.value = '';
	}

	submitForm($ev, value: any) {
        $ev.preventDefault();
		this.toolbox.FORM_markAllAsTouched(this.valForm)

        if (this.valForm.valid) {
			this.loading = true

			this.media.name = value.name
			this.media.type = value.type
			this.media.text = value.text
			this.media.condition = value.condition
			this.media.poiID = this.poi.id
			this.media.file = value.file

			if (this.isModify) {
				this.mediasManager.modify(this.media).subscribe( data => {
					if (data.result) {
						this.router.navigate(["/place/" + this.poi.place.id + "/POIs/" + this.poi.id + "/medias"])
					}

					this.loading = false
				})
			} else {
				this.mediasManager.add(this.media).subscribe( data => {
					if (data.result) {
						this.router.navigate(["/place/" + this.poi.place.id + "/POIs/" + this.poi.id + "/medias"])
					}

					this.loading = false
				})
			}
		}
    }
}
