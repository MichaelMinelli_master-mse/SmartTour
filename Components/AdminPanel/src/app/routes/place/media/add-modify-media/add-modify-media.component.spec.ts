import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddModifyMediaComponent } from './add-modify-media.component';

describe('AddModifyMediaComponent', () => {
  let component: AddModifyMediaComponent;
  let fixture: ComponentFixture<AddModifyMediaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddModifyMediaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddModifyMediaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
