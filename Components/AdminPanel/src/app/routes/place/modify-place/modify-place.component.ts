import { Place } from './../../../model/Place';
import { PlacesManagerService } from './../../../model/places-manager/places-manager.service';
import { ActivatedRoute, Router } from '@angular/router';

import { SessionManagerService } from './../../../model/session-manager/session-manager.service';
import { Component, OnInit } from '@angular/core';


@Component({
    selector: 'app-modify-place',
    templateUrl: './modify-place.component.html',
    styleUrls: ['./modify-place.component.scss']
})
export class ModifyPlaceComponent implements OnInit {

	public place: Place

    constructor(public placesManager: PlacesManagerService, public sessionManager: SessionManagerService, public route: ActivatedRoute, public router: Router) {
		this.route.params.subscribe(params => {
			let place = this.placesManager.allPlaces.find(element => { return element.id == params["id"] })

			if (place != undefined && place != null) {
				sessionManager.verifySession(true).subscribe(data => {
					if (!(data && (this.sessionManager.getUser().placeAdmin || this.sessionManager.getUser().places.find(place => place.id == params["id"]) != undefined))) {
						this.router.navigate(["/home"])
					} else {
						this.place = place
					}
				})
			} else {
				this.router.navigate(["/place/" + params["id"]])
			}
		});
	}

    ngOnInit() {
    }
}
