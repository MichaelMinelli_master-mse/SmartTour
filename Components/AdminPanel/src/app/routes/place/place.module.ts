import { AddModifyMediaComponent } from './media/add-modify-media/add-modify-media.component';
import { AddModifyPOIDialog } from './point-of-interest/pois/add-modify-poi-dialog/add-modify-poi-dialog.component';
import { POIsComponent } from './point-of-interest/pois/pois.component';
import { ModifyPlaceComponent } from './modify-place/modify-place.component';
import { AdminModule } from './../admin/admin.module';
import { PlaceComponent } from './place/place.component';
import { SharedModule } from './../../shared/shared.module';
import { CustomFormsModule } from 'ng2-validation';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MediasComponent } from './media/medias/medias.component';

const routes: Routes = [
	//Place
    { path: ':id', 				component: PlaceComponent },
	{ path: ':id/modify', 		component: ModifyPlaceComponent },

	//Point of Interest
	{ path: ':place/POIs', 		component: POIsComponent },

	//Medias
    { path: ':place/POIs/:poi/medias', 				component: MediasComponent },
    { path: ':place/POIs/:poi/medias/add', 			component: AddModifyMediaComponent },
    { path: ':place/POIs/:poi/medias/modify/:id', 	component: AddModifyMediaComponent }
];

@NgModule({
    imports: [
		SharedModule,
		RouterModule.forChild(routes),
		TranslateModule,
		CommonModule,
		CustomFormsModule,
		FormsModule,
		ReactiveFormsModule,
		AdminModule
    ],
    declarations: [
		PlaceComponent,
		ModifyPlaceComponent,
		POIsComponent,
		AddModifyPOIDialog,
		MediasComponent,
		AddModifyMediaComponent
	],
  	entryComponents: [
		AddModifyPOIDialog
	],
  	exports: [
		RouterModule,
		AddModifyPOIDialog
    ]
})
export class PlaceModule { }
