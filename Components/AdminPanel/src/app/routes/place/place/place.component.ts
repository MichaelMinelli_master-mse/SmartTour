import { PlacesManagerService } from './../../../model/places-manager/places-manager.service';
import { Place } from './../../../model/Place';
import { ActivatedRoute, Router } from '@angular/router';

import { SessionManagerService } from './../../../model/session-manager/session-manager.service';
import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';


@Component({
    selector: 'app-place',
    templateUrl: './place.component.html',
    styleUrls: ['./place.component.scss']
})
export class PlaceComponent implements OnInit {

	public place: Place = new Place()
	public loading: boolean = true

	public image: SafeStyle

    constructor(public placesManager: PlacesManagerService, public sessionManager: SessionManagerService, public route: ActivatedRoute, public router: Router, public sanitizer: DomSanitizer) {
		this.route.params.subscribe(params => {
			if (params["id"] != null || params["id"] != undefined) {
				sessionManager.verifySession(true).subscribe(data => {
					if (!(data && (this.sessionManager.getUser().placeAdmin || this.sessionManager.getUser().places.find(place => place.id == params["id"]) != undefined))) {
						this.router.navigate(["/home"])
					} else {
						this.placesManager.get(params["id"]).subscribe(place => {
							this.place = place
							this.loading = false

							this.image = this.sanitizer.bypassSecurityTrustStyle(`url(${place.imagePath})`);
						})
					}
				})
			}
		});
	}

    ngOnInit() {
	}

	modifyPlace() {
		this.placesManager.allPlaces = [this.place]
		this.router.navigate(["/place/" + String(this.place.id) + "/modify"])
	}

	pois() {
		this.placesManager.allPlaces = [this.place]
		this.router.navigate(["/place/" + String(this.place.id) + "/POIs"])
	}
}
