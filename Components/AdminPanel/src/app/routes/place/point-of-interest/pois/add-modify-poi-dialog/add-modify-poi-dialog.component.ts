import { POIsManagerService } from './../../../../../model/pois-manager/pois-manager.service';
import { ToolboxService } from './../../../../../core/toolbox/toolbox.service';
import { Place } from './../../../../../model/Place';
import { PointOfInterest } from './../../../../../model/PointOfInterest';
import { Validators } from '@angular/forms';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UsersManagerService } from './../../../../../model/users-manager/users-manager.service';
import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { User } from '../../../../../model/User';

@Component({
	selector: 'app-add-modify-poi-dialog',
	templateUrl: './add-modify-poi-dialog.component.html',
	styleUrls: ['./add-modify-poi-dialog.component.scss']
})
export class AddModifyPOIDialog {

	public loading: boolean = false

	public title: string = "pois.add-modify-dialog.title.ADD"
	public poi: PointOfInterest = null

	public place: Place

	public valForm: FormGroup;

	constructor(public dialogRef: MatDialogRef<AddModifyPOIDialog>, @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder, public poisManager: POIsManagerService, private toolbox: ToolboxService, public usersManager: UsersManagerService) {
		this.valForm = fb.group({
			'name': ["", Validators.required],
			'order': ["", Validators.required]
		});

		this.place = data.place

		if (data.poi) {
			this.title = "pois.add-modify-dialog.title.MODIFY"
			this.poi = data.poi

			this.valForm = fb.group({
				'name': [this.poi.name, Validators.required],
				'order': [this.poi.order, Validators.required]
			});
		}
	}

	onNoClick(): void {
		this.dialogRef.close();
	}

	submitForm($ev, value: any) {
		let isModify = this.poi != null

		if (this.poi == null)
			this.poi = new PointOfInterest()

		$ev.preventDefault();
        this.toolbox.FORM_markAllAsTouched(this.valForm)

        if (this.valForm.valid) {
			this.loading = true

			this.poi.name = value.name
			this.poi.order = value.order
			this.poi.placeID = this.place.id

			if (isModify) {
				this.poisManager.modify(this.poi).subscribe( data => {
					if (data.result) {
						this.dialogRef.close();
					}

					this.loading = false
				})
			} else {
				this.poisManager.add(this.poi).subscribe( data => {
					if (data.result) {
						this.dialogRef.close();
					}

					this.loading = false
				})
			}
        }
	}
}
