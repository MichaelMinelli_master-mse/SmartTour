import { AddModifyPOIDialog } from './add-modify-poi-dialog/add-modify-poi-dialog.component';
import { PlacesManagerService } from './../../../../model/places-manager/places-manager.service';
import { POIsManagerService } from './../../../../model/pois-manager/pois-manager.service';
import { PointOfInterest } from './../../../../model/PointOfInterest';
import { BackendProviderService } from './../../../../model/backend-provider/backend-provider.service';
import { Place } from './../../../../model/Place';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { User } from './../../../../model/User';

import { SessionManagerService } from '.././../../../model/session-manager/session-manager.service';
import { Component, OnInit } from '@angular/core';
import { ToolboxService } from '../../../../core/toolbox/toolbox.service';
import { MatDialog } from '@angular/material';

const swal = require('sweetalert');


@Component({
    selector: 'app-pois',
    templateUrl: './pois.component.html',
    styleUrls: ['./pois.component.scss']
})
export class POIsComponent implements OnInit {

	public place: Place
	public placeName: string = ""

	private allPOIs: Array<PointOfInterest> = []
	public pois: Array<PointOfInterest> = []

	public searchText: string = ""
	public loaded: boolean = false

    constructor(public sessionManager: SessionManagerService, public poisManager: POIsManagerService, public placesManager: PlacesManagerService, public backend: BackendProviderService, public translate: TranslateService, public router: Router, private route: ActivatedRoute, public toolbox: ToolboxService, public dialog: MatDialog) {
		this.route.params.subscribe(params => {
			this.place = this.placesManager.allPlaces.find(element => { return element.id == params["place"] })

			if (this.place != undefined && this.place != null) {
				this.placeName = this.place.name
				if (this.placeName.length > 24) {
					this.placeName = this.placeName.substr(0, 20) + "..."
				}

				sessionManager.verifySession(true).subscribe(data => {
					this.updatePlaces()
				})
			} else if (params["place"] != undefined && params["place"] != null) {
				this.router.navigate(["/place/" + params["place"]])
			}  else {
				this.router.navigate(["/home"])
			}
		});
	}

	navigate(url: string) {
		this.router.navigate([url])
	}

	updatePlaces() {
		this.loaded = false
		this.allPOIs = []
		this.pois = []
		this.poisManager.getAll(this.place).subscribe(
			pois => {
				this.allPOIs = this.pois = pois
				this.search(null)
				this.loaded = true
			}
		)
	}

	search(searchText: string) {
		if (searchText != null)
			this.searchText = searchText.toLowerCase()

		this.pois = this.allPOIs.filter( value => {
			return this.searchText == "" ? true : (value.name.toLowerCase().includes(this.searchText))
		})
	}

	private addModify(poi: PointOfInterest) {
		let dialogRef = this.dialog.open(AddModifyPOIDialog, {
			width: '500px',
			data: { place: this.place, poi: poi }
		});

		dialogRef.afterClosed().subscribe(_ => {
			this.updatePlaces()
		});
	}

	add() {
		this.addModify(null)
	}

	modify(poi: PointOfInterest) {
		this.addModify(poi)
	}

	attachments(poi: PointOfInterest) {
		this.poisManager.allPOIs = this.allPOIs.map( poi => { poi.place = this.place; return poi } )
		this.router.navigate(["/place/" + this.place.id + "/POIs/" + poi.id + "/medias"])
	}

	delete(poi: PointOfInterest) {
		swal({
			title: this.translate.instant("modal.delete.TITLE"),
			text: this.translate.instant("modal.delete.TEXT"),
			confirmButtonText: this.translate.instant("modal.delete.CONFIRM"),
            cancelButtonText: this.translate.instant("modal.delete.CANCEL"),
			icon: "warning",
			buttons: true,
			dangerMode: true,
		}).then((willDelete) => {
			if (willDelete) {
				this.poisManager.delete(poi).subscribe(data => {
					if (data.result) {
						let filter = value => { return value.id != poi.id }
						this.allPOIs = this.allPOIs.filter(filter)
						this.search(null)

						swal(this.translate.instant("modal.delete.CONFIRMATION"), {
							icon: "success",
						});
					}
				})
			}
		})
	}

    ngOnInit() {
    }
}
