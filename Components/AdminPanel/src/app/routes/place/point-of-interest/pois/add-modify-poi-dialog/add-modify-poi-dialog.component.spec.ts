import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddModifyPOIDialog } from './add-modify-poi-dialog.component';

describe('AddModifyPOIDialogComponent', () => {
  let component: AddAccessDialog;
  let fixture: ComponentFixture<AddModifyPOIDialog>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddModifyPOIDialog ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddModifyPOIDialog);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
