
import { SessionManagerService } from './../../../model/session-manager/session-manager.service';
import { Component, OnInit } from '@angular/core';


@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    constructor(public sessionManager: SessionManagerService) {
		sessionManager.verifySessionFromPages()
	}

	openURL(url: string) {

	}

    ngOnInit() {
    }
}
