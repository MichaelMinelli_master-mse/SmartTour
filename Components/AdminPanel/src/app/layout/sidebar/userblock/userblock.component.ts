import { User } from './../../../model/User';
import { UsersManagerService } from './../../../model/users-manager/users-manager.service';
import { SessionManagerService } from './../../../model/session-manager/session-manager.service';
import { SettingsService } from './../../../core/settings/settings.service';
import { Component, OnInit } from '@angular/core';

import { UserblockService } from './userblock.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-userblock',
    templateUrl: './userblock.component.html',
    styleUrls: ['./userblock.component.scss']
})
export class UserblockComponent implements OnInit {

    constructor(private userblockService: UserblockService, private sessionManager: SessionManagerService, private usersManager: UsersManagerService, private router: Router) { }

    ngOnInit() {
    }

    userBlockIsVisible() {
        return this.userblockService.getVisibility();
    }

	modifyProfile() {
		this.usersManager.allUsers = [this.sessionManager.getUser()]
		this.router.navigate(["/admin/users/modify/", this.sessionManager.getUser().mail])
	}

	modifyPassword() {
		this.usersManager.allUsers = [this.sessionManager.getUser()]
		this.router.navigate(["/admin/users/password/", this.sessionManager.getUser().mail])
	}
}
