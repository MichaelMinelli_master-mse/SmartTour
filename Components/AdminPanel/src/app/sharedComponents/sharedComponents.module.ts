import { NgModule } from '@angular/core';

import { LanguageSelectorComponent } from './language-selector/language-selector.component';

@NgModule({
    imports: [

    ],
    declarations: [
		LanguageSelectorComponent
	],
    exports: [
		LanguageSelectorComponent
    ]
})

export class SharedComponentsModule {
    constructor() {
;
    }
}
