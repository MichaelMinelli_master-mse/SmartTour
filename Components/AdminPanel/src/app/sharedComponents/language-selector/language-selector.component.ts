import { Component, OnInit } from '@angular/core';
import { TranslatorService } from '../../core/translator/translator.service';

@Component({
  selector: 'app-language-selector',
  templateUrl: './language-selector.component.html',
  styleUrls: ['./language-selector.component.scss']
})
export class LanguageSelectorComponent implements OnInit {

	constructor(public translator: TranslatorService) { }

	ngOnInit() {
	}

	setLang(value) {
		this.translator.useLanguage(value);
	}
}
