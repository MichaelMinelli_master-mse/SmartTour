echo '********************************************* Kill previous API'
pkill SmartTour_API

echo '********************************************* Update git repository'
cd SmartTour
git pull

echo '********************************************* Start API'
cd Components/API/
swift build
nohup swift run SmartTour_API RELEASE &

echo '********************************************* API started'
kill $(ps aux | grep 'sshd:' | awk '{print $2}' | head -1)
