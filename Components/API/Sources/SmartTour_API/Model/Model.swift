//
//  Model.swift
//  SmartTour_APIPackageDescription
//
//  Created by Michaël Minelli on 04.04.18.
//

import Foundation
import PerfectHTTP

protocol Model {
    var _lightVersion: Bool { get }
    
    static func printAll(response: HTTPResponse?)
}
