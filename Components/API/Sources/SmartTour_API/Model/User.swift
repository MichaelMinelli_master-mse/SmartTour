import PerfectLib
import StORM
import MySQLStORM
import PerfectHTTP

class User: MySQLStORM, JSONConvertible, Model {
    
    // NOTE: First param in class should be the ID.
    var userMail            : String    = ""
    var userFirstName       : String    = ""
    var userLastName        : String    = ""
    var userPassword        : String    = ""
    var userPasswordSalt    : String    = ""
    var userPlaceAdmin      : Int8      = 0
    var _userPlaceAdmin     : Bool {
        get {
            return userPlaceAdmin != 0
        }
    }
    var _places             : [Place]   {
        get {
            if _userPlaceAdmin {
                let place = Place()
                try? place.findAll()
                return place.rows().map({ (place) -> Place in
                    place._lightVersion = true
                    return place
                })
            } else {
                return Access.getAll(for: self).map({ access in access._place })
            }
        }
    }
    
    var _lightVersion: Bool = false
    
    init(lightVersion: Bool = false) {
        super.init()
        
        self._lightVersion = lightVersion
    }
    
    init(mail: String, firstName: String, lastName: String, password: String, passwordSalt: String? = nil, placeAdmin: Bool) {
        super.init()
        
        self.userMail = mail
        self.userFirstName = firstName
        self.userLastName = lastName
        self.userPlaceAdmin = placeAdmin ? 1 : 0
        
        if let passwordSalt = passwordSalt {
            self.userPassword = password
            self.userPasswordSalt = passwordSalt
        } else {
            self.generatePassword(from: password)
        }
    }
    
    init(mail: String) {
        super.init()
        
        self.userMail = mail
    }
    
    override open func table() -> String { return "User" }

    override func to(_ this: StORMRow) {
        userMail                = this.data["userMail"]         as? String        ?? ""
        userFirstName           = this.data["userFirstName"]    as? String        ?? ""
        userLastName            = this.data["userLastName"]     as? String        ?? ""
        userPassword            = this.data["userPassword"]     as? String        ?? ""
        userPasswordSalt        = this.data["userPasswordSalt"] as? String        ?? ""
        userPlaceAdmin          = this.data["userPlaceAdmin"]    as? Int8          ?? 0
    }
    
    func rows() -> [User] {
        var rows = [User]()
        for i in 0..<self.results.rows.count {
            let row = User()
            row.to(self.results.rows[i])
            rows.append(row)
        }
        return rows
    }
    
    public func getJSONValues() -> [String : Any] {
        var result: [String: Any] = [
            "mail": self.userMail,
            "firstName": self.userFirstName,
            "lastName": self.userLastName,
            "placeAdmin": self._userPlaceAdmin
        ]
        
        if !_lightVersion {
            result["places"] = self._places
        }
        
        return result
    }
    
    func jsonEncodedString() throws -> String {
        return try self.getJSONValues().jsonEncodedString()
    }
    
    func updateValues(firstName: String, lastName: String, mail: String, placeAdmin: Bool) -> Bool {
        do {
            try self.update(data: [("userFirstName", firstName), ("userLastName", lastName), ("userMail", mail), ("userPlaceAdmin", placeAdmin ? 1 : 0)], idName: "userMail", idValue: userMail)
            
            self.userFirstName = firstName
            self.userLastName = lastName
            self.userMail = mail
            self.userPlaceAdmin = placeAdmin ? 1 : 0
            
            return true
        } catch {
            return false
        }
    }
    
    func updatePassword(_ password: String) -> Bool {
        do {
            self.userPassword = getEncryptedPassword(from: password)
            
            try self.save()
            
            return true
        } catch {
            return false
        }
    }
    
    func getEncryptedPassword(from unencryptedPassword: String) -> String {
        if let digestBytes = (unencryptedPassword + userPasswordSalt).digest(.sha512), let hexBytes = digestBytes.encode(.hex), let hexBytesStr = String(validatingUTF8: hexBytes) {
            return hexBytesStr
        }
        
        return ""
    }
    
    func checkPassword(unencryptedPassword: String) -> Bool {
        return getEncryptedPassword(from: unencryptedPassword) == userPassword
    }
    
    func generatePassword(from unencryptedPassword: String) {
        self.userPasswordSalt = ToolBox.randomString(length: Config.shared.USER_PASSWORD_SALT_LENGTH)
        
        self.userPassword = getEncryptedPassword(from: unencryptedPassword)
    }
    
    public static func printAll(response: HTTPResponse? = nil) {
        let t = User()
        let cursor = StORMCursor(limit: 9999999,offset: 0)
        try? t.select(
            columns: [],
            whereclause: "true",
            params: [],
            orderby: ["userMail"],
            cursor: cursor
        )
        
        if let response = response {
            response.appendBody(string: try! t.rows().jsonEncodedString())
        } else {
            print(try! t.rows().jsonEncodedString())
        }
    }
}
