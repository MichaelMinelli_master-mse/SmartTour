import PerfectLib
import StORM
import MySQLStORM
import PerfectHTTP

class Access: MySQLStORM, JSONConvertible, Model {
    
    // NOTE: First param in class should be the ID.
    var accessUser             : String  = ""
    var accessPlace            : Int32   = 0
    
    var _lightVersion: Bool = false
    
    var _user: User {
        get {
            let user = User(lightVersion: true)
            try? user.get(accessUser)
            return user
        }
    }
    
    var _place: Place {
        get {
            let place = Place(lightVersion: true)
            try? place.get(accessPlace)
            return place
        }
    }
    
    override init() {
        super.init()
    }
    
    init(user: String, place: Int32) {
        super.init()
        
        self.accessUser = user
        self.accessPlace = place
    }
    
    override open func table() -> String { return "Access" }
    
    override func to(_ this: StORMRow) {
        accessUser                    = this.data["accessUser"]         as? String              ?? ""
        accessPlace                   = this.data["accessPlace"]        as? Int32               ?? -1
    }
    
    func rows() -> [Access] {
        var rows = [Access]()
        for i in 0..<self.results.rows.count {
            let row = Access()
            row.to(self.results.rows[i])
            rows.append(row)
        }
        return rows
    }
    
    public func getJSONValues() -> [String : Any] {
        return [
            "user": self.accessUser,
            "place": self.accessPlace
        ]
    }
    
    func jsonEncodedString() throws -> String {
        return try self.getJSONValues().jsonEncodedString()
    }
    
    public func find() -> Access? {
        let access = Access()
        try? access.select(
            columns: [],
            whereclause: "accessUser='\(self.accessUser)' AND accessPlace='\(self.accessPlace)'",
            params: [],
            orderby: [],
            cursor: StORMCursor(limit: 9999999,offset: 0)
        )
        return access.rows().count > 0 ? access.rows()[0] : nil
    }
    
    public static func getAll(for user: User) -> [Access] {
        let access = Access()
        try? access.select(
            columns: [],
            whereclause: "accessUser='\(user.userMail)'",
            params: [],
            orderby: [],
            cursor: StORMCursor(limit: 9999999,offset: 0)
        )
        return access.rows()
    }
    
    public static func getAll(for place: Place) -> [Access] {
        let access = Access()
        try? access.select(
            columns: [],
            whereclause: "accessPlace=\(place.placeID)",
            params: [],
            orderby: [],
            cursor: StORMCursor(limit: 9999999,offset: 0)
        )
        return access.rows()
    }
    
    public static func printAll(response: HTTPResponse? = nil) {
        let t = Access()
        let cursor = StORMCursor(limit: 9999999,offset: 0)
        try? t.select(
            columns: [],
            whereclause: "true",
            params: [],
            orderby: ["accessUser"],
            cursor: cursor
        )
        
        for row in t.rows() {
            do {
                if let response = response {
                    response.appendBody(string: try row.jsonEncodedString())
                } else {
                    print(try row.jsonEncodedString())
                }
            } catch {
                
            }
        }
        
        if let response = response {
            response.appendBody(string: try! t.rows().jsonEncodedString())
        } else {
            print(try! t.rows().jsonEncodedString())
        }
    }
}


