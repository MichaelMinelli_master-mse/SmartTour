import PerfectLib
import StORM
import MySQLStORM
import PerfectHTTP

class Media: MySQLStORM, JSONConvertible, Model {
    
    // NOTE: First param in class should be the ID.
    var mediaID             : Int          = 0
    var mediaName           : String       = ""
    var mediaType           : Int          = 0 //1->Texte, 2->Photo, 3->Video, 4->Audio
    var mediaExtension      : String       = ""
    var mediaText           : String       = ""
    var mediaCondition      : String       = ""
    var mediaPOI            : Int          = 0
    var _mediaText          : Array<UInt8> {
        get {
            return Array(mediaText.utf8)
        }
        set {
            self.mediaText = newValue.toString(.utf8, defaultValue: "")
        }
    }
    var _mediaCondition     : Array<UInt8> {
        get {
            return Array(mediaCondition.utf8)
        }
        set {
            self.mediaCondition = newValue.toString(.utf8, defaultValue: "")
        }
    }
    
    var _lightVersion: Bool = false
    
    init(lightVersion: Bool = false) {
        super.init()
        
        self._lightVersion = lightVersion
    }
    
    init(id: Int) {
        super.init()
        
        self.mediaID = id
    }
    
    convenience init(id: Int=0, name: String, type: Int, ext: String, text: String, condition: String, poi: Int) {
        self.init(id: id)
        
        self.mediaName = name
        self.mediaType = type
        self.mediaExtension = ext
        self.mediaText = text
        self.mediaCondition = condition
        self.mediaPOI = poi
    }
    
    override open func table() -> String { return "Media" }
    
    override func to(_ this: StORMRow) {
        mediaID                     = Int(this.data["mediaID"]          as? Int32               ?? -1)
        mediaName                   = this.data["mediaName"]            as? String              ?? ""
        mediaType                   = Int(this.data["mediaType"]        as? Int32               ?? -1)
        mediaExtension              = this.data["mediaExtension"]       as? String              ?? ""
        _mediaText                  = this.data["mediaText"]            as? Array<UInt8>        ?? Array<UInt8>()
        _mediaCondition             = this.data["mediaCondition"]       as? Array<UInt8>        ?? Array<UInt8>()
        mediaPOI                    = Int(this.data["mediaPOI"]         as? Int32               ?? -1)
    }
    
    func rows() -> [Media] {
        var rows = [Media]()
        for i in 0..<self.results.rows.count {
            let row = Media()
            row.to(self.results.rows[i])
            rows.append(row)
        }
        return rows
    }
    
    public func getJSONValues() -> [String : Any] {
        var result: [String: Any] = [
            "id": self.mediaID,
            "name": self.mediaName,
            "type": self.mediaType,
            "extension": self.mediaExtension,
            "text": self.mediaText,
            "condition": self.mediaCondition,
            "poiID": self.mediaPOI
        ]
        
        if !_lightVersion {
            
        }
        
        return result
    }
    
    func jsonEncodedString() throws -> String {
        return try self.getJSONValues().jsonEncodedString()
    }
    
    public static func printAll(response: HTTPResponse? = nil) {
        let t = Media()
        let cursor = StORMCursor(limit: 9999999,offset: 0)
        try? t.select(
            columns: [],
            whereclause: "true",
            params: [],
            orderby: ["mediaID"],
            cursor: cursor
        )
        
        for row in t.rows() {
            do {
                if let response = response {
                    response.appendBody(string: try row.jsonEncodedString())
                } else {
                    print(try row.jsonEncodedString())
                }
            } catch {
                
            }
        }
        
        if let response = response {
            response.appendBody(string: try! t.rows().jsonEncodedString())
        } else {
            print(try! t.rows().jsonEncodedString())
        }
    }
}

