import PerfectLib
import StORM
import MySQLStORM
import PerfectHTTP

class PointOfInterest: MySQLStORM, JSONConvertible, Model {
    
    // NOTE: First param in class should be the ID.
    var poiID               : Int32?    = nil
    var poiName             : String    = ""
    var poiOrder            : Int32     = 0
    var poiLatitude         : Double    = 0.0
    var poiLongitude        : Double    = 0.0
    var poiPlace            : Int32     = -1
    
    var _lightVersion: Bool = false
    
    var _place: Place {
        get {
            let place = Place(lightVersion: true)
            try? place.get(poiPlace)
            return place
        }
    }
    
    override init() {
        super.init()
    }
    
    init(id: Int32) {
        super.init()
        
        self.poiID = id
    }
    
    init(name: String, order: Int32, latitude: Double, longitude: Double, place: Int32) {
        super.init()
        
        self.poiName = name
        self.poiOrder = order
        self.poiLatitude = latitude
        self.poiLongitude = longitude
        self.poiPlace = place
    }
    
    convenience init(id: Int32, name: String, order: Int32, latitude: Double, longitude: Double, place: Int32) {
        self.init(name: name, order: order, latitude: latitude, longitude: longitude, place: place)
        
        self.poiID = id
    }
    
    override open func table() -> String { return "PointOfInterest" }
    
    override func to(_ this: StORMRow) {
        poiID                    = this.data["poiID"]           as? Int32               ?? -1
        poiName                  = this.data["poiName"]         as? String              ?? ""
        poiOrder                 = this.data["poiOrder"]        as? Int32               ?? 0
        poiLatitude              = this.data["poiLatitude"]     as? Double              ?? 0.0
        poiLongitude             = this.data["poiLongitude"]    as? Double              ?? 0.0
        poiPlace                 = this.data["poiPlace"]        as? Int32               ?? -1
    }
    
    func rows() -> [PointOfInterest] {
        var rows = [PointOfInterest]()
        for i in 0..<self.results.rows.count {
            let row = PointOfInterest()
            row.to(self.results.rows[i])
            rows.append(row)
        }
        return rows
    }
    
    public func getJSONValues() -> [String : Any] {
        var result: [String: Any] = [
            "id"        : self.poiID,
            "name"      : self.poiName,
            "order"     : self.poiOrder,
            "latitude"  : self.poiLatitude,
            "longitude" : self.poiLongitude,
            "placeID"   : self.poiPlace
        ]
        
        if !_lightVersion {
            result["place"] = self._place
        }
        
        return result
    }
    
    func jsonEncodedString() throws -> String {
        return try self.getJSONValues().jsonEncodedString()
    }
    
    public static func getAll(for place: Place) -> [PointOfInterest] {
        let pot = PointOfInterest()
        try? pot.select(
            columns: [],
            whereclause: "poiPlace='\(place.placeID)'",
            params: [],
            orderby: ["poiOrder"],
            cursor: StORMCursor(limit: 9999999,offset: 0)
        )
        return pot.rows()
    }
    
    func updateValues(name: String, order: Int32, latitude: Double, longitude: Double) -> Bool {
        if let poiID = poiID {
            do {
                try self.update(data: [("poiName", name), ("poiOrder", order), ("poiLatitude", latitude), ("poiLongitude", longitude)], idName: "poiID", idValue: poiID)
                
                self.poiName = name
                self.poiOrder = order
                self.poiLatitude = latitude
                self.poiLongitude = longitude
                
                return true
            } catch {
                return false
            }
        } else {
            return false
        }
    }
    
    public static func printAll(response: HTTPResponse? = nil) {
        let t = PointOfInterest()
        let cursor = StORMCursor(limit: 9999999,offset: 0)
        try? t.select(
            columns: [],
            whereclause: "true",
            params: [],
            orderby: ["poiPlace, poiOrder"],
            cursor: cursor
        )
        
        for row in t.rows() {
            do {
                if let response = response {
                    response.appendBody(string: try row.jsonEncodedString())
                } else {
                    print(try row.jsonEncodedString())
                }
            } catch {
                
            }
        }
        
        if let response = response {
            response.appendBody(string: try! t.rows().jsonEncodedString())
        } else {
            print(try! t.rows().jsonEncodedString())
        }
    }
}


