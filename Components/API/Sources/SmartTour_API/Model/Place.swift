import PerfectLib
import StORM
import MySQLStORM
import PerfectHTTP

class Place: MySQLStORM, JSONConvertible, Model {
    
    // NOTE: First param in class should be the ID.
    var placeID             : Int32        = 0
    var placeName           : String       = ""
    var placeDescription    : Array<UInt8> = Array<UInt8>()
    var placeImageExtension : String       = ""
    var _placeDescription   : String {
        get {
            return placeDescription.toString(.utf8, defaultValue: "")
        }
        set {
            self.placeDescription = Array(newValue.utf8)
        }
    }
    var _users              : [User] {
        get {
            return Access.getAll(for: self).map({ access -> User in access._user })
        }
    }
    
    var _lightVersion: Bool = false
    
    init(lightVersion: Bool = false) {
        super.init()
        
        self._lightVersion = lightVersion
    }
    
    init(id: Int32) {
        super.init()
        
        self.placeID = id
    }
    
    init(name: String, description: String, imageExtension: String) {
        super.init()
        
        self.placeName = name
        self._placeDescription = description
        self.placeImageExtension = imageExtension
    }
    
    convenience init(id: Int, name: String, description: String, imageExtension: String) {
        self.init(name: name, description: description, imageExtension: imageExtension)
        
        self.placeID = Int32(id)
    }
    
    override open func table() -> String { return "Place" }
    
    override func to(_ this: StORMRow) {
        placeID                     = this.data["placeID"]              as? Int32               ?? -1
        placeName                   = this.data["placeName"]            as? String              ?? ""
        placeDescription            = this.data["placeDescription"]     as? Array<UInt8>        ?? Array<UInt8>()
        placeImageExtension         = this.data["placeImageExtension"]  as? String              ?? ""
    }
    
    func rows() -> [Place] {
        var rows = [Place]()
        for i in 0..<self.results.rows.count {
            let row = Place()
            row.to(self.results.rows[i])
            rows.append(row)
        }
        return rows
    }
    
    public func getJSONValues() -> [String : Any] {
        var result: [String: Any] = [
            "id": self.placeID,
            "name": self.placeName,
            "description": self._placeDescription,
            "imageExtension": self.placeImageExtension
        ]
        
        if !_lightVersion {
            result["users"] = self._users
        }
        
        return result
    }
    
    func jsonEncodedString() throws -> String {
        return try self.getJSONValues().jsonEncodedString()
    }
    
    override open func create() throws {
        do {
            placeID = Int32(try insert([("placeName", placeName), ("placeDescription", _placeDescription), ("placeImageExtension", placeImageExtension)]) as? Int ?? -1)
        } catch {
            throw StORMError.error("\(error)")
        }
    }
    
    func updateValues(placeName: String, placeDescription: String, placeImageExtension: String) -> Bool {
        do {
            try self.update(data: [("placeName", placeName), ("placeDescription", placeDescription), ("placeImageExtension", placeImageExtension)], idName: "placeID", idValue: self.placeID)
            
            self.placeName = placeName
            self._placeDescription = placeDescription
            self.placeImageExtension = placeImageExtension
            
            return true
        } catch {
            return false
        }
    }
    
    public static func printAll(response: HTTPResponse? = nil) {
        let t = Place()
        let cursor = StORMCursor(limit: 9999999,offset: 0)
        try? t.select(
            columns: [],
            whereclause: "true",
            params: [],
            orderby: ["placeID"],
            cursor: cursor
        )
        
        for row in t.rows() {
            do {
                if let response = response {
                    response.appendBody(string: try row.jsonEncodedString())
                } else {
                    print(try row.jsonEncodedString())
                }
            } catch {
                
            }
        }
        
        if let response = response {
            response.appendBody(string: try! t.rows().jsonEncodedString())
        } else {
            print(try! t.rows().jsonEncodedString())
        }
    }
}

