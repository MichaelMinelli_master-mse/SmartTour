import StORM
import MySQLStORM
//import PerfectSessionMySQL

class Config {
    static var shared = Config()

    private init() { }

    var RELEASE = false

    var PORT: UInt16 {
        get {
            return RELEASE ? RELEASE_PORT : DEBUG_PORT
        }
    }
    private let RELEASE_PORT: UInt16 = 80
    private let DEBUG_PORT: UInt16 = 8181

    var DOMAIN: String {
        get {
            return RELEASE ? RELEASE_DOMAIN : DEBUG_DOMAIN
        }
    }
    private let RELEASE_DOMAIN = "smarttour.michaelminelli.ch"
    private let DEBUG_DOMAIN = "localhost"
    
    var DOCUMENT_ROOT: String {
        get {
            return RELEASE ? RELEASE_DOCUMENT_ROOT : DEBUG_DOCUMENT_ROOT
        }
    }
    private let RELEASE_DOCUMENT_ROOT = "/root/SmartTour/Components/API/webroot/"
    private let DEBUG_DOCUMENT_ROOT = "/Users/michaelminelli/Dev/SmartTour/Components/API/webroot/"

    let LOG_FILE = "./log.log"
    
    let USER_PASSWORD_SALT_LENGTH = 32
    
    func configMySQL() {
        let HOST                        = "sql01.michaelminelli.ch"
        let USERNAME                    = "SmartTour"
        let PASSWORD                    = "SmartTour_Pass_4_MySQL"
        let DATABASE                    = "hesso_pa_smarttour"
        let PORT                        = 3306
        
        MySQLConnector.host             = HOST
        MySQLConnector.username         = USERNAME
        MySQLConnector.password         = PASSWORD
        MySQLConnector.database         = DATABASE
        MySQLConnector.port             = PORT
        
        MySQLConnector.charset          = "utf8"
    }
}
