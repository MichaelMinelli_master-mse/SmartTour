//
//  MySQL_ToolBox.swift
//  SmartTour_APIPackageDescription
//
//  Created by Michaël Minelli on 04.04.18.
//

import Foundation
import PerfectLib
import StORM
import MySQLStORM

class MySQL_Toolbox {
    private init() {}
    
    public static func getString(from row: StORMRow, for key: String, with encoding: String.Encoding, defaultValue: String = "") -> String {
        let valueBytes = row.data[key] as? Array<UInt8> ?? Array<UInt8>()
        
        return valueBytes.toString(encoding, defaultValue: defaultValue)
    }
    
    public static func getBool(from row: StORMRow, for key: String, defaultValue: Bool = false) -> Bool {
        let valueInt = row.data[key] as? Int8 ?? (defaultValue ? 1 : 0)
        
        return valueInt != 0
    }
}
