//
//  Globals.swift
//  SmartTour_APIPackageDescription
//
//  Created by Michaël Minelli on 03.04.18.
//

import Foundation

class ToolBox {
    private init() { }
    
    static func randomString(length: Int) -> String {
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            #if os(Linux)
                let rand = UInt32(random()) % len
            #else
                let rand = arc4random_uniform(len)
            #endif
                
            randomString += String(format: "%c", letters.character(at: Int(rand)))
        }
        
        return randomString
    }
}

extension Array where Element == UInt8 {
    func toString(_ encoding: String.Encoding, defaultValue: String) -> String {
        return String(bytes: self, encoding: encoding) ?? defaultValue
    }
}

extension Formatter {
    static let iso8601: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
        return formatter
    }()
}
extension Date {
    var iso8601: String {
        return Formatter.iso8601.string(from: self)
    }
}

extension String {
    var dateFromISO8601: Date? {
        return Formatter.iso8601.date(from: self)   // "Mar 22, 2017, 10:22 AM"
    }
}
