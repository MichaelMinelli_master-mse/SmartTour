import Foundation
import PerfectLib
import PerfectHTTP

class BackendResponse: JSONConvertibleObject {
    enum GlobalErrorCode: Int {
        case UNKNOWN = -1000
        case NO_SESSION = -1001
        case NOT_LOGIN = -1002
        case INSUFFICIENT_PERMISSION = -1003
    }
    
    let result: Bool
    let code: Int
    private let _description: String?
    let timestamp: String
    let data: JSONConvertible?
    let sessionToken: String
    
    init(request: HTTPRequest, result: Bool, code: Int? = nil, globalCode: GlobalErrorCode? = nil, description: String? = nil, data: JSONConvertible? = nil) {
        self.result = result
        self.code = code ?? globalCode?.rawValue ?? (result ? 200 : -1000)
        self._description = description
        self.timestamp = Date().iso8601
        self.data = data ?? ""
        self.sessionToken = request.session?.token ?? ""
    }
    
    var description: String {
        if let description = _description {
            return description
        } else if result == true {
            return "Ok"
        } else {
            switch GlobalErrorCode(rawValue: code) {
            case .none:
                return "Unknown error."
            case let error where error == .UNKNOWN:
                return "Unknown error."
            case let error where error == .NO_SESSION:
                return "No session can be created."
            case let error where error == .NOT_LOGIN:
                return "User not logged in."
            case let error where error == .INSUFFICIENT_PERMISSION:
                return "Insufficient permissions."
            default:
                return ""
            }
        }
    }
    
    override open func getJSONValues() -> [String:Any] {
        return [
            "result": self.result,
            "code": self.code,
            "description": self.description,
            "timestamp": self.timestamp,
            "data": self.data ?? "",
            "sessionToken": self.sessionToken
        ]
    }
    
    func send(overHTTP response: HTTPResponse) {
        response.setHeader(.contentType, value: "application/json")
        
        do {
            try response.appendBody(string: self.jsonEncodedString())
        } catch { }
        
        response.completed()
    }
}
