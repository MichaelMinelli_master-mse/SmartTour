//
//  main.swift
//  PerfectTemplate
//
//  Created by Kyle Jessup on 2015-11-05.
//	Copyright (C) 2015 PerfectlySoft, Inc.
//
//===-----var--------------------------------------------------------------===//
//
// This source file is part of the Perfect.org open source project
//
// Copyright (c) 2015 - 2016 PerfectlySoft Inc. and the Perfect project authors
// Licensed under Apache License v2.0
//
// See http://perfect.org/licensing.html for license information
//
//===----------------------------------------------------------------------===//
//

import PerfectLib
import PerfectHTTP
import PerfectHTTPServer
import PerfectSession
import PerfectSessionSQLite
import SQLiteStORM
import PerfectRequestLogger

//////////////////////////////////////////////////////////////////////////// Command line arguments
for argument in CommandLine.arguments {
    switch (argument) {
    case "RELEASE":
        Config.shared.RELEASE = true
    case "DEBUG":
        Config.shared.RELEASE = false
    default: break
    }
}



//////////////////////////////////////////////////////////////////////////// MySQL config
Config.shared.configMySQL()



//////////////////////////////////////////////////////////////////////////// Web Server Init
let server = HTTPServer()
server.documentRoot = Config.shared.DOCUMENT_ROOT

// V1 routes
var api1Routes = Routes(baseUri: "/v1")
api1Routes.add(Router.shared.routes)
server.addRoutes(api1Routes)

server.serverPort = Config.shared.PORT



//////////////////////////////////////////////////////////////////////////// HTTP Session settings

SessionConfig.name = "SmartTourAPI"
SessionConfig.idle = 86400
SessionConfig.cookieSameSite = .strict
SessionConfig.purgeInterval = 3600

SessionConfig.CORS.enabled = true
SessionConfig.CORS.acceptableHostnames = ["*"]
SessionConfig.CORS.maxAge = 3600
SessionConfig.CORS.methods = HTTPMethod.allMethods
SessionConfig.CORS.customHeaders = ["Authorization"] //Allow Authorization header for session token by bearer

SQLiteConnector.db = "./SmartTour_SessionDB"

//StORMdebug = true
let sessionDriver = SessionSQLiteDriver()
server.setRequestFilters([sessionDriver.requestFilter])
server.setResponseFilters([sessionDriver.responseFilter])


//////////////////////////////////////////////////////////////////////////// Request logger instantiation

if !Config.shared.RELEASE {
    RequestLogFile.location = Config.shared.LOG_FILE
    
    // Instantiate a logger
    let myLogger = RequestLogger()

    // Add the filters
    // Request filter at high priority to be executed first
    server.setRequestFilters([(myLogger, .high)])
    // Response filter at low priority to be executed last
    server.setResponseFilters([(myLogger, .low)])
}



//////////////////////////////////////////////////////////////////////////// Web Server

do {
	// Launch the HTTP server.
	try server.start()
} catch PerfectError.networkError(let err, let msg) {
	print("Network error thrown: \(err) \(msg)")
}
