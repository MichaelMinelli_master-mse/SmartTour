import PerfectLib
import PerfectHTTP
import PerfectHTTPServer

//https://restfulapi.net/http-methods/

class Router {
    static let shared = Router()

    private init() { }

    var routes: Routes {
        get {
			var routes = Routes()

            //Base route
			routes.add(method: .get, uri: "/", handler: WebHandlers.shared.baseURL)
            
            //User Session
            routes.add(method: .get, uri: "/login/{mail}/{password}",   handler: WebHandlers.shared.login)
            routes.add(method: .get, uri: "/logout",                    handler: WebHandlers.shared.logout)
            routes.add(method: .get, uri: "/restore_session",           handler: WebHandlers.shared.restoreSession)
            
            //Users
            routes.add(method: .get,    uri: "/users",                  handler: WebHandlers.shared.users)
            routes.add(method: .post,   uri: "/users",                  handler: WebHandlers.shared.userAdd)
            routes.add(method: .put,    uri: "/users/{mail}",           handler: WebHandlers.shared.userModify)
            routes.add(method: .put,    uri: "/users/{mail}/password",  handler: WebHandlers.shared.userPassword)
            routes.add(method: .delete, uri: "/users/{mail}",           handler: WebHandlers.shared.userDelete)
            
            //Places
            routes.add(method: .get,    uri: "/places/{id}",    handler: WebHandlers.shared.place)
            routes.add(method: .get,    uri: "/places",         handler: WebHandlers.shared.places)
            routes.add(method: .post,   uri: "/places",         handler: WebHandlers.shared.placeAdd)
            routes.add(method: .put,    uri: "/places/{id}",    handler: WebHandlers.shared.placeModify)
            routes.add(method: .delete, uri: "/places/{id}",    handler: WebHandlers.shared.placeDelete)
            
            //User / Place
            routes.add(method: .post,   uri: "/places/{id}/add-user",             handler: WebHandlers.shared.placeUserAdd)
            routes.add(method: .delete, uri: "/places/{id}/remove-user/{mail}",   handler: WebHandlers.shared.placeUserDelete)
            
            //PointOfInterest
            routes.add(method: .get,    uri: "/places/{place}/POIs",            handler: WebHandlers.shared.POIs)
            routes.add(method: .post,   uri: "/places/{place}/POIs",            handler: WebHandlers.shared.POIsAdd)
            routes.add(method: .put,    uri: "/places/{place}/POIs/{id}",       handler: WebHandlers.shared.POIsModify)
            routes.add(method: .delete, uri: "/places/{place}/POIs/{id}",       handler: WebHandlers.shared.POIsDelete)
            
            //Media
            routes.add(method: .get,    uri: "/medias",                         handler: WebHandlers.shared.medias)
            routes.add(method: .post,   uri: "/medias",                         handler: WebHandlers.shared.mediaAdd)
            routes.add(method: .put,    uri: "/medias/{id}",                    handler: WebHandlers.shared.mediaModify)
            routes.add(method: .delete, uri: "/medias/{id}",                    handler: WebHandlers.shared.mediaDelete)
            
            //https://nehalist.io/uploading-files-in-angular2/
            
            //DEV
            routes.add(method: .get, uri: "/debug", handler: WebHandlers.shared.debug)
            
            //CORS Requests OPTIONS
            routes.add(method: .options, uri: "/**") { (_, response) in response.completed() }

			return routes
		}
	}
}
