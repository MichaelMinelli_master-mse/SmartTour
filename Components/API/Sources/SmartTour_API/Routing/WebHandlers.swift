import PerfectLib
import PerfectHTTP
import PerfectSession

class WebHandlers {
    static let shared = WebHandlers()

    private init() { }
    
    //Base route
    let baseURL: (HTTPRequest, HTTPResponse) -> () = {(request, response) in BackendResponse(request: request, result: true).send(overHTTP: response) }
    
    //User Session
    let login: (HTTPRequest, HTTPResponse) -> () = LoginManager.shared.login
    let logout: (HTTPRequest, HTTPResponse) -> () = LoginManager.shared.logout
    let restoreSession: (HTTPRequest, HTTPResponse) -> () = LoginManager.shared.restoreSession
    
    //Users
    let users: (HTTPRequest, HTTPResponse) -> () = UsersManager.shared.getAll
    let userAdd: (HTTPRequest, HTTPResponse) -> () = UsersManager.shared.add
    let userModify: (HTTPRequest, HTTPResponse) -> () = UsersManager.shared.modify
    let userPassword: (HTTPRequest, HTTPResponse) -> () = UsersManager.shared.password
    let userDelete: (HTTPRequest, HTTPResponse) -> () = UsersManager.shared.delete
    
    //Places
    let place: (HTTPRequest, HTTPResponse) -> () = PlacesManager.shared.get
    let places: (HTTPRequest, HTTPResponse) -> () = PlacesManager.shared.getAll
    let placeAdd: (HTTPRequest, HTTPResponse) -> () = PlacesManager.shared.add
    let placeModify: (HTTPRequest, HTTPResponse) -> () = PlacesManager.shared.modify
    let placeDelete: (HTTPRequest, HTTPResponse) -> () = PlacesManager.shared.delete
    
    //Access
    let placeUserAdd: (HTTPRequest, HTTPResponse) -> () = AccessManager.shared.add
    let placeUserDelete: (HTTPRequest, HTTPResponse) -> () = AccessManager.shared.delete
    
    //PointOfInterest
    let POIs: (HTTPRequest, HTTPResponse) -> () = PointOfInterestManager.shared.getAll
    let POIsAdd: (HTTPRequest, HTTPResponse) -> () = PointOfInterestManager.shared.add
    let POIsModify: (HTTPRequest, HTTPResponse) -> () = PointOfInterestManager.shared.modify
    let POIsDelete: (HTTPRequest, HTTPResponse) -> () = PointOfInterestManager.shared.delete
    
    //Media
    let medias: (HTTPRequest, HTTPResponse) -> () = MediasManager.shared.getAll
    let mediaAdd: (HTTPRequest, HTTPResponse) -> () = MediasManager.shared.add
    let mediaModify: (HTTPRequest, HTTPResponse) -> () = MediasManager.shared.modify
    let mediaDelete: (HTTPRequest, HTTPResponse) -> () = MediasManager.shared.delete

    func debug(request: HTTPRequest, response: HTTPResponse) {
        // Respond with a simple message.
        response.setHeader(.contentType, value: "application/json")
        
        User.printAll(response: response)
        
        response.completed()
    }
}
