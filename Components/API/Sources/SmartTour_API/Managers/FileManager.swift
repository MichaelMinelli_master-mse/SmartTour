//
//  FileManager.swift
//  COpenSSL
//
//  Created by Michaël Minelli on 19.04.18.
//

import Foundation
import PerfectLib

class FileManager {
    static let shared = FileManager()
    
    private init() { }
    
    let PLACES_IMAGES = Config.shared.DOCUMENT_ROOT + "places/images/"
    let MEDIAS_FILES  = Config.shared.DOCUMENT_ROOT + "medias/"
    
    func getFileExtension(from fileName: String?) -> String? {
        guard let fileName = fileName else {
            return nil
        }
        
        let fileNameParts = fileName.split(".")
        return fileNameParts.last
    }
    
    private func move(file: File, to destination: String, overWrite: Bool = true) {
        do {
            let _ = try file.moveTo(path: destination, overWrite: overWrite)
        } catch {
            print(error)
        }
    }
    
    func move(file: File, with fileExtension: String, for place: Place) {
        move(file: file, to: self.PLACES_IMAGES + "\(place.placeID).\(fileExtension)")
    }
    
    func move(file: File, with fileExtension: String, for media: Media) {
        move(file: file, to: self.MEDIAS_FILES + "\(media.mediaID).\(fileExtension)")
    }
    
    func remove(image place: Place) {
        let file = File(self.PLACES_IMAGES + "\(place.placeID).\(place.placeImageExtension)")
        file.delete()
    }
    
    func remove(file media: Media) {
        let file = File(self.MEDIAS_FILES + "\(media.mediaID).\(media.mediaExtension)")
        file.delete()
    }
}
