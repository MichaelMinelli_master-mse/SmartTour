import PerfectLib
import PerfectHTTP
import PerfectSession

class AccessManager {
    static let shared = AccessManager()
    
    private init() { }
    
    // Errors -> -1: Not enough params or params empty/incorrect
    func add(request: HTTPRequest, response: HTTPResponse) {
        var addResponse: BackendResponse!
        
        defer {
            addResponse.send(overHTTP: response)
        }
        
        let login = LoginManager.shared.checkAdminAccess(for: request)
        guard login.result else {
            addResponse = login
            return
        }
        
        ///////////// Verify params
        guard let _id = request.urlVariables["id"], let id = Int(_id) else {
            addResponse =  BackendResponse(request: request, result: false, code: -1, description: "Not enough params")
            return
        }
        
        let _mail = request.param(name: "mail", defaultValue: "")
        guard let mail = _mail, !mail.isEmpty else {
            addResponse =  BackendResponse(request: request, result: false, code: -1, description: "Not enough params")
            return
        }
        
        let access = Access(user: mail, place: Int32(id))
        do {
            try access.create()
            addResponse = BackendResponse(request: request, result: true)
        } catch {
            addResponse =  BackendResponse(request: request, result: false, globalCode: .UNKNOWN)
        }
    }
    
    // Errors -> -1: Not enough params or params empty/incorrect
    func delete(request: HTTPRequest, response: HTTPResponse) {
        var deleteResponse: BackendResponse!
        
        defer {
            deleteResponse.send(overHTTP: response)
        }
        
        let login = LoginManager.shared.checkAdminAccess(for: request)
        guard login.result else {
            deleteResponse = login
            return
        }
        
        ///////////// Verify params
        guard let _id = request.urlVariables["id"], let mail = request.urlVariables["mail"], let id = Int(_id), !mail.isEmpty else {
            deleteResponse =  BackendResponse(request: request, result: false, code: -1, description: "Not enough params")
            return
        }
        
        let access = Access(user: mail, place: Int32(id))
        do {
            try access.delete()
            deleteResponse = BackendResponse(request: request, result: true)
        } catch {
            deleteResponse =  BackendResponse(request: request, result: false, globalCode: .UNKNOWN)
        }
    }
    
    func isExist(mail: String, place: Int32) -> Bool {
        let access = Access(user: mail, place: place).find()
        return access != nil ? true : false
    }
}
