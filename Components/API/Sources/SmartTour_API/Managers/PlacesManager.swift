import PerfectLib
import PerfectHTTP
import PerfectSession

class PlacesManager {
    static let shared = PlacesManager()
    
    private init() { }
    
    func get(by id: Int) -> Place? {
        let place = Place()
        
        do {
            try place.get(id)
            
            guard !place.keyIsEmpty() else {
                return nil
            }
            
            return place
        } catch {
            return nil
        }
    }
    
    func get(request: HTTPRequest, response: HTTPResponse) {
        var placeResponse: BackendResponse!
        
        defer {
            placeResponse.send(overHTTP: response)
        }
        
        var lightVersion: Bool = true
        request.queryParams.forEach { (param) in
            switch param.0 {
            case "lightVersion":
                lightVersion = Bool(param.1) ?? lightVersion
            default:
                break;
            }
        }
        
        if !lightVersion {
            let login = LoginManager.shared.checkAdminAccess(for: request)
            guard login.result else {
                placeResponse = login
                return
            }
        }
        
        guard let _id = request.urlVariables["id"], let id = Int(_id) else {
            placeResponse =  BackendResponse(request: request, result: false, code: -1, description: "Not enough params")
            return
        }
        
        guard let place = self.get(by: id) else {
            placeResponse =  BackendResponse(request: request, result: false, code: -2, description: "Place not exist")
            return
        }
        
        place._lightVersion = lightVersion
        placeResponse = BackendResponse(request: request, result: true, data: ["place": place])
    }
    
    //Params: lightVersion: Optionnal = true
    func getAll(request: HTTPRequest, response: HTTPResponse) {
        var placesResponse: BackendResponse!
        
        defer {
            placesResponse.send(overHTTP: response)
        }
        
        var lightVersion: Bool = true
        request.queryParams.forEach { (param) in
            switch param.0 {
            case "lightVersion":
                lightVersion = Bool(param.1) ?? lightVersion
            default:
                break;
            }
        }
        
        if !lightVersion {
            let login = LoginManager.shared.checkAdminAccess(for: request)
            guard login.result else {
                placesResponse = login
                return
            }
        }
        
        let places = Place()
        try? places.findAll()
        placesResponse = BackendResponse(request: request, result: true, data: ["places": places.rows().map({ (place) -> Place in place._lightVersion = lightVersion; return place })])
    }
    
    private func getUploadedPicture(for request: HTTPRequest) -> MimeReader.BodySpec? {
        if let upload = request.postFileUploads?.first(where: { upload -> Bool in upload.fieldName == "picture" }) {
            return upload
        }
        
        return nil
    }
 
    // Params -> name, description
    // Errors -> -1: Not enough params or params empty/incorrect
    func add(request: HTTPRequest, response: HTTPResponse) {
        var addResponse: BackendResponse!
        
        defer {
            addResponse.send(overHTTP: response)
        }
        
        let login = LoginManager.shared.checkAdminAccess(for: request)
        guard login.result else {
            addResponse = login
            return
        }
        
        let _name = request.param(name: "name", defaultValue: "")
        let _description = request.param(name: "description", defaultValue: "")
        
        guard let name = _name, let description = _description, let picture = getUploadedPicture(for: request), let pictureFile = picture.file, let pictureExtension = FileManager.shared.getFileExtension(from: picture.fileName), !name.isEmpty, !description.isEmpty else {
            addResponse =  BackendResponse(request: request, result: false, code: -1, description: "Not enough params")
            return
        }
        
        let place = Place(name: name, description: description, imageExtension: pictureExtension)
        do {
            try place.create()
            
            FileManager.shared.move(file: pictureFile, with: pictureExtension, for: place)
            
            LoginManager.shared.reloadSession(for: request)
            
            addResponse = BackendResponse(request: request, result: true)
        } catch {
            addResponse =  BackendResponse(request: request, result: false, globalCode: .UNKNOWN)
        }
    }
    
    // Params -> name, description
    // Errors -> -1: Not enough params or params empty/incorrect, -2: Place not exists
    func modify(request: HTTPRequest, response: HTTPResponse) {
        var modifyResponse: BackendResponse!
        
        defer {
            modifyResponse.send(overHTTP: response)
        }
        
        ///////////// Verify params
        guard let _id = request.urlVariables["id"], let id = Int(_id) else {
            modifyResponse =  BackendResponse(request: request, result: false, code: -1, description: "Not enough params")
            return
        }
        
        guard let place = self.get(by: id) else {
            modifyResponse =  BackendResponse(request: request, result: false, code: -2, description: "Place not exist")
            return
        }
        
        let _name = request.param(name: "name", defaultValue: "")
        let _description = request.param(name: "description", defaultValue: "")
        
        guard let name = _name, let description = _description, !name.isEmpty, !description.isEmpty else {
            modifyResponse =  BackendResponse(request: request, result: false, code: -1, description: "Not enough params")
            return
        }
        
        let picture = getUploadedPicture(for: request)
        let pictureFile = picture?.file
        let pictureExtension = FileManager.shared.getFileExtension(from: picture?.fileName) ?? place.placeImageExtension
        
        ////////// Verify access
        let login = LoginManager.shared.isManager(place: place, for: request)
        guard login.result else {
            modifyResponse = login
            return
        }
        
        ////////// Update
        if place.updateValues(placeName: name, placeDescription: description, placeImageExtension: pictureExtension) {
            if let pictureFile = pictureFile {
                FileManager.shared.move(file: pictureFile, with: pictureExtension, for: place)
            }
            
            LoginManager.shared.reloadSession(for: request)
            
            modifyResponse = BackendResponse(request: request, result: true)
        } else {
            modifyResponse =  BackendResponse(request: request, result: false, globalCode: .UNKNOWN)
        }
    }
    
    func delete(request: HTTPRequest, response: HTTPResponse) {
        var deleteResponse: BackendResponse!
        
        defer {
            deleteResponse.send(overHTTP: response)
        }
        
        let login = LoginManager.shared.checkAdminAccess(for: request)
        guard login.result else {
            deleteResponse = login
            return
        }
        
        guard let _id = request.urlVariables["id"], let id = Int32(_id) else {
            deleteResponse =  BackendResponse(request: request, result: false, code: -1, description: "Not enough params")
            return
        }
        
        guard let place = self.get(by: Int(id)) else {
            deleteResponse =  BackendResponse(request: request, result: false, code: -2, description: "Place not exist")
            return
        }

        do {
            try place.delete()
            
            FileManager.shared.remove(image: place)
            
            LoginManager.shared.reloadSession(for: request)
            
            deleteResponse = BackendResponse(request: request, result: true)
        } catch {
            deleteResponse =  BackendResponse(request: request, result: false, globalCode: .UNKNOWN)
        }
    }
    
    func isManager(place: Place, for request: HTTPRequest) -> Bool {
        let login = LoginManager.shared.checkAdminAccess(for: request)
        guard let userSession = request.session!.data["User"] as? [String: Any], let mail = userSession["mail"] as? String, login.result || AccessManager.shared.isExist(mail: mail, place: place.placeID) else {
            return false
        }
        
        return true
    }
}

