import PerfectLib
import PerfectHTTP
import PerfectSession

class MediasManager {
    static let shared = MediasManager()
    
    private init() { }
    
    func get(by id: Int) -> Media? {
        let media = Media()
        
        do {
            try media.get(id)
            
            guard !media.keyIsEmpty() else {
                return nil
            }
            
            return media
        } catch {
            return nil
        }
    }
    
    //Params: lightVersion: Optionnal = true, poi: Optionnal = all
    func getAll(request: HTTPRequest, response: HTTPResponse) {
        var mediasResponse: BackendResponse!
        
        defer {
            mediasResponse.send(overHTTP: response)
        }
        
        var lightVersion: Bool = true
        var poi: String = "all"
        request.queryParams.forEach { (param) in
            switch param.0 {
            case "lightVersion":
                lightVersion = Bool(param.1) ?? lightVersion
            case "poi":
                poi = param.1
            default:
                break;
            }
        }
        
        let medias = Media()
        if poi == "all" {
            try? medias.findAll()
        } else {
            try? medias.find(["mediaPOI": poi])
        }
        mediasResponse = BackendResponse(request: request, result: true, data: ["medias": medias.rows().map({ (media) -> Media in media._lightVersion = lightVersion; return media })])
    }

    private func getUploadedFile(for request: HTTPRequest) -> MimeReader.BodySpec? {
        if let file = request.postFileUploads?.first(where: { upload -> Bool in upload.fieldName == "file" }) {
            return file
        }
        
        return nil
    }
    
    // Params -> name, type, text, condition, poi
    // Errors -> -1: Not enough params or params empty/incorrect
    func add(request: HTTPRequest, response: HTTPResponse) {
        var addResponse: BackendResponse!
        
        defer {
            addResponse.send(overHTTP: response)
        }
        
        let login = LoginManager.shared.verifyLogin(for: request)
        guard login.result else {
            addResponse = login
            return
        }
        
        let _name = request.param(name: "name", defaultValue: "")
        let __type = request.param(name: "type", defaultValue: "")
        let _text = request.param(name: "text", defaultValue: "")
        let _condition = request.param(name: "condition", defaultValue: "")
        let __poi = request.param(name: "poi", defaultValue: "")
        
        guard let name = _name, let _type = __type, let text = _text, let condition = _condition, let _poi = __poi, let type = Int(_type), let poi = Int(_poi), !name.isEmpty, !condition.isEmpty else {
            addResponse =  BackendResponse(request: request, result: false, code: -1, description: "Not enough params")
            return
        }
        
        let file = getUploadedFile(for: request)
        let fileFile = file?.file
        let fileExtension = FileManager.shared.getFileExtension(from: file?.fileName) ?? ""
        
        let media = Media(name: name, type: type, ext: fileExtension, text: text, condition: condition, poi: poi)
        do {
            try media.save { id in
                if let id = id as? Int {
                    media.mediaID = id
                    
                    if let fileFile = fileFile {
                        FileManager.shared.move(file: fileFile, with: fileExtension, for: media)
                    }
                }
            }
            
            addResponse = BackendResponse(request: request, result: true)
        } catch {
            addResponse =  BackendResponse(request: request, result: false, globalCode: .UNKNOWN)
        }
    }

    // Params -> name, type, text, condition, poi
    // Errors -> -1: Not enough params or params empty/incorrect, -2: Media not exists
    func modify(request: HTTPRequest, response: HTTPResponse) {
        var modifyResponse: BackendResponse!
        
        defer {
            modifyResponse.send(overHTTP: response)
        }
        
        ///////////// Verify params
        guard let _id = request.urlVariables["id"], let id = Int(_id) else {
            modifyResponse =  BackendResponse(request: request, result: false, code: -1, description: "Not enough params")
            return
        }
        
        guard let media = self.get(by: id) else {
            modifyResponse =  BackendResponse(request: request, result: false, code: -2, description: "Place not exist")
            return
        }
        
        let _name = request.param(name: "name", defaultValue: "")
        let __type = request.param(name: "type", defaultValue: "")
        let _text = request.param(name: "text", defaultValue: "")
        let _condition = request.param(name: "condition", defaultValue: "")
        let __poi = request.param(name: "poi", defaultValue: "")
        
        guard let name = _name, let _type = __type, let text = _text, let condition = _condition, let _poi = __poi, let type = Int(_type), let poi = Int(_poi), !name.isEmpty, !condition.isEmpty else {
            modifyResponse =  BackendResponse(request: request, result: false, code: -1, description: "Not enough params")
            return
        }
        
        let file = getUploadedFile(for: request)
        let fileFile = file?.file
        let fileExtension = FileManager.shared.getFileExtension(from: file?.fileName) ?? media.mediaExtension
        
        if let _ = fileFile {
            FileManager.shared.remove(file: media)
        }
        
        ////////// Verify access
        let login = LoginManager.shared.verifyLogin(for: request)
        guard login.result else {
            modifyResponse = login
            return
        }
        
        ////////// Update
        media.mediaName = name
        media.mediaType = type
        media.mediaText = text
        media.mediaCondition = condition
        media.mediaPOI = poi
        media.mediaExtension = fileExtension
        do {
            try media.save()
            
            if let fileFile = fileFile {
                FileManager.shared.move(file: fileFile, with: fileExtension, for: media)
            }
            
            modifyResponse = BackendResponse(request: request, result: true)
        } catch {
            modifyResponse =  BackendResponse(request: request, result: false, globalCode: .UNKNOWN)
        }
    }

    func delete(request: HTTPRequest, response: HTTPResponse) {
        var deleteResponse: BackendResponse!
        
        defer {
            deleteResponse.send(overHTTP: response)
        }
        
        let login = LoginManager.shared.verifyLogin(for: request)
        guard login.result else {
            deleteResponse = login
            return
        }
        
        guard let _id = request.urlVariables["id"], let id = Int(_id) else {
            deleteResponse =  BackendResponse(request: request, result: false, code: -1, description: "Not enough params")
            return
        }
        
        guard let media = self.get(by: id) else {
            deleteResponse =  BackendResponse(request: request, result: false, code: -2, description: "Media not exist")
            return
        }
        
        do {
            try media.delete()
            
            FileManager.shared.remove(file: media)
            
            deleteResponse = BackendResponse(request: request, result: true)
        } catch {
            deleteResponse =  BackendResponse(request: request, result: false, globalCode: .UNKNOWN)
        }
    }
}

