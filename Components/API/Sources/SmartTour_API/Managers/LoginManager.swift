import PerfectLib
import PerfectHTTP
import PerfectSession

class LoginManager {
    static let shared = LoginManager()
    
    private init() { }
    
    //Code = -1: Not all params, -2: No user with this mail, -3: Wrong password, -4: No session can be found
    func login(request: HTTPRequest, response: HTTPResponse) {
        var loginResponse: BackendResponse!
        
        defer {
            loginResponse.send(overHTTP: response)
        }
        
        guard let mail = request.urlVariables["mail"], let password = request.urlVariables["password"] else {
            loginResponse =  BackendResponse(request: request, result: false, code: -1, description: "Not enough params", data: nil)
            return
        }
        
        let user = User()
        loginResponse = BackendResponse(request: request, result: false, code: -2, description: "User not found", data: nil)
        do {
            try user.get(mail)
            
            if user.keyIsEmpty() {
                return
            }
        } catch {
            return
        }
        
        if (user.checkPassword(unencryptedPassword: password)) {
            guard let _ = request.session else {
                loginResponse = BackendResponse(request: request, result: false, globalCode: .NO_SESSION, data: nil)
                return
            }
            
            request.session!.data["User"] = user
            
            loginResponse = BackendResponse(request: request, result: true, data: ["user": user])
        } else {
            loginResponse = BackendResponse(request: request, result: false, code: -3, description: "Wrong password", data: nil)
        }
    }
    
    func reloadSession(for request: HTTPRequest) {
        guard let userSession = request.session!.data["User"] as? [String: Any], let mail = userSession["mail"] as? String else {
            return
        }
        
        let user = User()
        do {
            try user.get(mail)
            
            if user.keyIsEmpty() {
                return
            }
        } catch {
            return
        }
        
        request.session!.data["User"] = user
    }
    
    func logout(request: HTTPRequest, response: HTTPResponse) {
        var paramToSave: [String: Any?] = ["csrf": nil]
        
        var logoutResponse: BackendResponse!
        
        defer {
            logoutResponse.send(overHTTP: response)
        }
        
        guard let _ = request.session else {
            logoutResponse = BackendResponse(request: request, result: false, globalCode: .NO_SESSION, data: nil)
            return
        }
        
        for key in paramToSave.keys {
            paramToSave[key] = request.session!.data[key]
        }
        request.session!.data.removeAll()
        paramToSave.forEach { (key: String, value: Any?) in
            request.session!.data[key] = value
        }
        
        logoutResponse = BackendResponse(request: request, result: true)
    }
    
    func verifyLogin(for request: HTTPRequest) -> BackendResponse {
        //Verify session
        guard let _ = request.session else {
            return BackendResponse(request: request, result: false, globalCode: .NO_SESSION, data: nil)
        }
        
        //Verify login
        if let user = request.session!.data["User"] {
            return BackendResponse(request: request, result: true, data: ["user": user])
        } else {
            return BackendResponse(request: request, result: false, globalCode: .NOT_LOGIN, data: nil)
        }
    }
    
    func checkAdminAccess(for request: HTTPRequest) -> BackendResponse {
        let response = LoginManager.shared.verifyLogin(for: request)
        
        guard response.result else {
            return response
        }
        
        if let user = request.session!.data["User"] as? [String: Any], let placeAdmin = user["placeAdmin"] as? Bool, placeAdmin {
            return response
        } else {
            return BackendResponse(request: request, result: false, globalCode: .INSUFFICIENT_PERMISSION)
        }
    }
    
    func isManager(place: Place, for request: HTTPRequest) -> BackendResponse {
        let login = LoginManager.shared.checkAdminAccess(for: request)
        if let userSession = request.session!.data["User"] as? [String: Any], let mail = userSession["mail"] as? String, login.result || AccessManager.shared.isExist(mail: mail, place: place.placeID) {
            return BackendResponse(request: request, result: true)
        } else {
            return BackendResponse(request: request, result: false, globalCode: .INSUFFICIENT_PERMISSION)
        }
    }
    
    func isSameUser(for request: HTTPRequest, with mail: String) -> Bool {
        var isSameUser: Bool = false
        if let loginUser = request.session!.data["User"] as? [String: Any], let loginUserMail = loginUser["mail"] as? String, loginUserMail == mail {
            isSameUser = true
        }
        
        return isSameUser
    }
    
    func checkAdminAccessOrSameUser(for request: HTTPRequest, with mail: String, additionalCondition: Bool = true) -> BackendResponse? {
        let login = LoginManager.shared.checkAdminAccess(for: request)
        
        return (login.result || (isSameUser(for: request, with: mail) && additionalCondition)) ? nil : login
    }
    
    func restoreSession(request: HTTPRequest, response: HTTPResponse) {
        reloadSession(for: request)
        verifyLogin(for: request).send(overHTTP: response)
    }
}
