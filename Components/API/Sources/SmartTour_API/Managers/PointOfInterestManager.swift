import PerfectLib
import PerfectHTTP
import PerfectSession

class PointOfInterestManager {
    static let shared = PointOfInterestManager()
    
    private init() { }
    
    //Params: lightVersion: Optionnal = true
    func getAll(request: HTTPRequest, response: HTTPResponse) {
        var potResponse: BackendResponse!
        
        defer {
            potResponse.send(overHTTP: response)
        }
        
        guard let _placeID = request.urlVariables["place"], let placeID = Int32(_placeID) else {
            potResponse =  BackendResponse(request: request, result: false, code: -1, description: "Not enough params")
            return
        }
        
        var lightVersion: Bool = true
        request.queryParams.forEach { (param) in
            switch param.0 {
            case "lightVersion":
                lightVersion = Bool(param.1) ?? lightVersion
            default:
                break;
            }
        }
        
        let pots = PointOfInterest.getAll(for: Place(id: placeID))
        potResponse = BackendResponse(request: request, result: true, data: ["POIs": pots.map({ (pot) -> PointOfInterest in pot._lightVersion = lightVersion; return pot })])
    }
    
    // Params -> name, order, latitude, longitude, place
    // Errors -> -1: Not enough params or params empty/incorrect
    func add(request: HTTPRequest, response: HTTPResponse) {
        var addResponse: BackendResponse!
        
        defer {
            addResponse.send(overHTTP: response)
        }
        
        let _name = request.param(name: "name", defaultValue: "")
        let __order = request.param(name: "order", defaultValue: "")
        let __latitude = request.param(name: "latitude", defaultValue: "")
        let __longitude = request.param(name: "longitude", defaultValue: "")
        
        guard let name = _name, let _order = __order, let _latitude = __latitude, let _longitude = __longitude, let _place = request.urlVariables["place"], let order = Int32(_order), let latitude = Double(_latitude), let longitude = Double(_longitude), let place = Int32(_place), !name.isEmpty() else {
            addResponse =  BackendResponse(request: request, result: false, code: -1, description: "Not enough params")
            return
        }
        
        let login = LoginManager.shared.isManager(place: Place(id: place), for: request)
        guard login.result else {
            addResponse = login
            return
        }
        
        let poi = PointOfInterest(name: name, order: order, latitude: latitude, longitude: longitude, place: place)
        do {
            try poi.create()
            
            addResponse = BackendResponse(request: request, result: true)
        } catch {
            addResponse =  BackendResponse(request: request, result: false, globalCode: .UNKNOWN)
        }
    }
    
    // Params -> name, order, latitude, longitude, place
    // Errors -> -1: Not enough params or params empty/incorrect, -2: Place not exists
    func modify(request: HTTPRequest, response: HTTPResponse) {
        var modifyResponse: BackendResponse!
        
        defer {
            modifyResponse.send(overHTTP: response)
        }
        
        ///////////// Verify params
        let _name = request.param(name: "name", defaultValue: "")
        let __order = request.param(name: "order", defaultValue: "")
        let __latitude = request.param(name: "latitude", defaultValue: "")
        let __longitude = request.param(name: "longitude", defaultValue: "")
        
        guard let name = _name, let _order = __order, let _latitude = __latitude, let _longitude = __longitude, let _place = request.urlVariables["place"], let _id = request.urlVariables["id"], let order = Int32(_order), let latitude = Double(_latitude), let longitude = Double(_longitude), let place = Int32(_place), let id = Int32(_id), !name.isEmpty() else {
            modifyResponse =  BackendResponse(request: request, result: false, code: -1, description: "Not enough params")
            return
        }
        
        let poi = PointOfInterest()
        let placeNotExist = BackendResponse(request: request, result: false, code: -2, description: "Place not exists")
        do {
            try poi.get(id)
            
            guard !poi.keyIsEmpty() && poi.poiPlace == place else {
                modifyResponse = placeNotExist
                return
            }
        } catch {
            modifyResponse = placeNotExist
            return
        }
        
        let login = LoginManager.shared.isManager(place: Place(id: place), for: request)
        guard login.result else {
            modifyResponse = login
            return
        }
        
        ////////// Update
        if poi.updateValues(name: name, order: order, latitude: latitude, longitude: longitude) {
            modifyResponse = BackendResponse(request: request, result: true)
        } else {
            modifyResponse =  BackendResponse(request: request, result: false, globalCode: .UNKNOWN)
        }
    }
    
    func delete(request: HTTPRequest, response: HTTPResponse) {
        var deleteResponse: BackendResponse!
        
        defer {
            deleteResponse.send(overHTTP: response)
        }
        
        guard let _placeID = request.urlVariables["place"], let _id = request.urlVariables["id"], let placeID = Int32(_placeID), let id = Int32(_id) else {
            deleteResponse =  BackendResponse(request: request, result: false, code: -1, description: "Not enough params")
            return
        }
        
        let login = LoginManager.shared.isManager(place: Place(id: placeID), for: request)
        guard login.result else {
            deleteResponse = login
            return
        }
        
        let poi = PointOfInterest(id: id)
        do {
            try poi.delete()
            deleteResponse = BackendResponse(request: request, result: true)
        } catch {
            deleteResponse =  BackendResponse(request: request, result: false, globalCode: .UNKNOWN)
        }
    }
}
