import PerfectLib
import PerfectHTTP
import PerfectSession

class UsersManager {
    static let shared = UsersManager()
    
    private init() { }
    
    func get(by mail: String) -> User? {
        let user = User()
        
        do {
            try user.get(mail)
            
            guard !user.keyIsEmpty() else {
                return nil
            }
            
            return user
        } catch {
            return nil
        }
    }
    
    //Params: lightVersion: Optionnal = true
    func getAll(request: HTTPRequest, response: HTTPResponse) {
        var usersResponse: BackendResponse!
        
        defer {
            usersResponse.send(overHTTP: response)
        }
        
        let login = LoginManager.shared.checkAdminAccess(for: request)
        guard login.result else {
            usersResponse = login
            return
        }
        
        var lightVersion: Bool = true
        request.queryParams.forEach { (param) in
            switch param.0 {
            case "lightVersion":
                lightVersion = Bool(param.1) ?? lightVersion
            default:
                break;
            }
        }
        
        let users = User()
        try? users.findAll()
        usersResponse = BackendResponse(request: request, result: true, data: ["users": users.rows().map({ (user) -> User in user._lightVersion = lightVersion; return user })])
    }
    
    // Params -> firstName, lastName, mail, admin: "true" | "false", password
    // Errors -> -1: Not enough params or params empty/incorrect, -2: User already exists
    func add(request: HTTPRequest, response: HTTPResponse) {
        var addResponse: BackendResponse!
        
        defer {
            addResponse.send(overHTTP: response)
        }
        
        let login = LoginManager.shared.checkAdminAccess(for: request)
        guard login.result else {
            addResponse = login
            return
        }
        
        let _firstName = request.param(name: "firstName", defaultValue: "")
        let _lastName = request.param(name: "lastName", defaultValue: "")
        let _mail = request.param(name: "mail", defaultValue: "")
        let __admin = request.param(name: "admin", defaultValue: "")
        let _password = request.param(name: "password", defaultValue: "")
        
        guard let firstName = _firstName, let lastName = _lastName, let mail = _mail, let _admin = __admin, let password = _password, !firstName.isEmpty, !lastName.isEmpty, !firstName.isEmpty, let admin = Bool(_admin), !password.isEmpty else {
            addResponse =  BackendResponse(request: request, result: false, code: -1, description: "Not enough params")
            return
        }
        
        guard self.get(by: mail) == nil else {
            addResponse =  BackendResponse(request: request, result: false, code: -2, description: "User already exists")
            return
        }
        
        let user = User(mail: mail, firstName: firstName, lastName: lastName, password: password, placeAdmin: admin)
        do {
            try user.create()
            addResponse = BackendResponse(request: request, result: true)
        } catch {
            addResponse =  BackendResponse(request: request, result: false, globalCode: .UNKNOWN)
        }
    }
    
    // Params -> firstName, lastName, mail, admin: "true" | "false"
    // Errors -> -1: Not enough params or params empty/incorrect, -2: User not exists, -3: Mail already taken
    func modify(request: HTTPRequest, response: HTTPResponse) {
        var modifyResponse: BackendResponse!
        
        defer {
            modifyResponse.send(overHTTP: response)
        }
        
        ///////////// Verify params
        guard let oldMail = request.urlVariables["mail"], !oldMail.isEmpty else {
            modifyResponse =  BackendResponse(request: request, result: false, code: -1, description: "Not enough params")
            return
        }
        
        guard let user = self.get(by: oldMail) else {
            modifyResponse =  BackendResponse(request: request, result: false, code: -2, description: "User not exist")
            return
        }
        
        let _firstName = request.param(name: "firstName", defaultValue: "")
        let _lastName = request.param(name: "lastName", defaultValue: "")
        let _mail = request.param(name: "mail", defaultValue: "")
        let __admin = request.param(name: "admin", defaultValue: "")
        
        guard let firstName = _firstName, let lastName = _lastName, let mail = _mail, let _admin = __admin, !firstName.isEmpty, !lastName.isEmpty, !firstName.isEmpty, let admin = Bool(_admin) else {
            modifyResponse =  BackendResponse(request: request, result: false, code: -1, description: "Not enough params")
            return
        }
        
        ////////// Verify access (same user OR admin)
        if let response = LoginManager.shared.checkAdminAccessOrSameUser(for: request, with: oldMail, additionalCondition: admin == user._userPlaceAdmin) {
            modifyResponse = response
            return
        }
        
        ////////// Verify mail change (id)
        if oldMail != mail {
            guard self.get(by: mail) == nil else {
                modifyResponse =  BackendResponse(request: request, result: false, code: -3, description: "User already exists")
                return
            }
        }
        
        ////////// Update
        if user.updateValues(firstName: firstName, lastName: lastName, mail: mail, placeAdmin: admin) {
            if LoginManager.shared.isSameUser(for: request, with: oldMail) {
                request.session!.data["User"] = user
            }
            
            modifyResponse = BackendResponse(request: request, result: true)
        } else {
            modifyResponse =  BackendResponse(request: request, result: false, globalCode: .UNKNOWN)
        }
    }
    
    // Params -> password
    // Errors -> -1: Not enough params or params empty/incorrect, -2: User not exists
    func password(request: HTTPRequest, response: HTTPResponse) {
        var passwordResponse: BackendResponse!
        
        defer {
            passwordResponse.send(overHTTP: response)
        }
        
        ///////////// Verify params
        guard let mail = request.urlVariables["mail"], !mail.isEmpty else {
            passwordResponse =  BackendResponse(request: request, result: false, code: -1, description: "Not enough params")
            return
        }
        
        guard let user = self.get(by: mail) else {
            passwordResponse =  BackendResponse(request: request, result: false, code: -2, description: "User not exist")
            return
        }
        
        let _password = request.param(name: "password", defaultValue: "")
        guard let password = _password, !password.isEmpty else {
            passwordResponse =  BackendResponse(request: request, result: false, code: -1, description: "Not enough params")
            return
        }
        
        ////////// Verify access (same user OR admin)
        if let response = LoginManager.shared.checkAdminAccessOrSameUser(for: request, with: mail) {
            passwordResponse = response
            return
        }
        
        ////////// Update
        if user.updatePassword(password) {
            if LoginManager.shared.isSameUser(for: request, with: mail) {
                request.session!.data["User"] = user
            }
            
            passwordResponse = BackendResponse(request: request, result: true)
        } else {
            passwordResponse =  BackendResponse(request: request, result: false, globalCode: .UNKNOWN)
        }
    }
    
    func delete(request: HTTPRequest, response: HTTPResponse) {
        var deleteResponse: BackendResponse!
        
        defer {
            deleteResponse.send(overHTTP: response)
        }
        
        let login = LoginManager.shared.checkAdminAccess(for: request)
        guard login.result else {
            deleteResponse = login
            return
        }
        
        guard let mail = request.urlVariables["mail"] else {
            deleteResponse =  BackendResponse(request: request, result: false, code: -1, description: "Not enough params")
            return
        }
        
        let user = User(mail: mail)
        do {
            try user.delete()
            deleteResponse = BackendResponse(request: request, result: true)
        } catch {
            deleteResponse =  BackendResponse(request: request, result: false, globalCode: .UNKNOWN)
        }
    }
}
