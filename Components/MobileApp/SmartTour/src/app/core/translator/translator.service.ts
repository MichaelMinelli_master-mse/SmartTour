import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Globalization } from '@ionic-native/globalization';
import { Config, Platform } from 'ionic-angular';

@Injectable()
export class TranslatorService {
    private defaultLanguage: string = 'fr';

    private availablelangs = [{ code: 'fr', text: 'Français' }, { code: 'en', text: 'English' }];

    constructor(public translate: TranslateService, private globalization: Globalization, public config: Config, public platform: Platform) {
        //this.globalization
        //.getPreferredLanguage()
        //.then(res => console.log(res))
        //.catch(e => console.log(e));

        if (!translate.getDefaultLang()) translate.setDefaultLang(this.defaultLanguage);

        this.useLanguage();
    }

    useLanguage(lang: string = null) {
        this.translate.use(lang || this.translate.getDefaultLang());

        this.platform.ready().then(() => {
            this.config.set('backButtonText', this.translate.instant('ui.general.BACK'));
        });
    }

    getAvailableLanguages() {
        return this.availablelangs;
    }
}
