import { Injectable } from '@angular/core';
import { Content, Refresher } from 'ionic-angular';

@Injectable()
export class ToolboxService {
    constructor() {}

    static IMG_randomParam(): string {
        return String(Math.floor(Math.random() * 10000000));
    }

    pullToRefresh(refresher: Refresher, content: Content) {
        refresher._top = content.contentTop + 'px';
        refresher.state = 'ready';
        refresher._onEnd();
    }
}
