import { User } from './../../model/User';
import { Injectable } from '@angular/core';
declare var $: any;

@Injectable()
export class SettingsService {
    public app: any;

    constructor() {
        // App Settings
        // -----------------------------------
        this.app = {
            name: 'SmartTour',
            description: 'SmartTour Admin Panel',
            copyright: 'Michaël Minelli',
            year: new Date().getFullYear()
        };
    }

    getAppSetting(name) {
        return name ? this.app[name] : this.app;
    }

    setAppSetting(name, value) {
        if (typeof this.app[name] !== 'undefined') {
            this.app[name] = value;
        }
    }

    getAge(): number {
        let age = localStorage.getItem('settings.AGE');
        return age ? +age : null;
    }

    setAge(age: number) {
        localStorage.setItem('settings.AGE', age.toString());
    }
}
