import { Injectable } from '@angular/core';
//import decode from 'jwt-decode';

//https://ryanchenkie.com/angular-authentication-using-the-http-client-and-http-interceptors

@Injectable()
export class AuthService {

	public getToken(): string {
		return localStorage.getItem('sessionToken')
	}

	public setToken(token: string) {
		localStorage.setItem('sessionToken', token)
	}

	/*
	public isAuthenticated(): boolean {
	// get the token
	const token = this.getToken();
	// return a boolean reflecting
	// whether or not the token is expired
	return tokenNotExpired(token);
	}
	*/

}
