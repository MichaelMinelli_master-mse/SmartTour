import { ModuleWithProviders, NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { Nl2BrPipeModule } from 'nl2br-pipe';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TapticEngine } from '@ionic-native/taptic-engine';
import { Globalization } from '@ionic-native/globalization';
import { StreamingMedia } from '@ionic-native/streaming-media';

import { ColorsService } from './colors/colors.service';

// https://angular.io/styleguide#!#04-10
@NgModule({
    imports: [TranslateModule, Nl2BrPipeModule],
    providers: [ColorsService, StatusBar, SplashScreen, TapticEngine, Globalization, StreamingMedia],
    declarations: [],
    exports: [TranslateModule, Nl2BrPipeModule]
})
// https://github.com/ocombe/ng2-translate/issues/209
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule
        };
    }
}
