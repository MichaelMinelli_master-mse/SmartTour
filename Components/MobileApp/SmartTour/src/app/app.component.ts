import { TranslatorService } from './core/translator/translator.service';
import { Component } from '@angular/core';
import { Platform, Config } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { SmartTourPage } from '../pages/smart-tour/smart-tour';
import { TranslateService } from '@ngx-translate/core';

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    rootPage: any = SmartTourPage;

    constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, tr: TranslatorService, public translate: TranslateService, public config: Config) {
        platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();

            this.config.set('backButtonText', this.translate.instant('ui.general.BACK'));
        });
    }
}
