import { TestBed, inject } from '@angular/core/testing';

import { BackendProviderService } from './backend-provider.service';

describe('BackendProviderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BackendProviderService]
    });
  });

  it('should be created', inject([BackendProviderService], (service: BackendProviderService) => {
    expect(service).toBeTruthy();
  }));
});
