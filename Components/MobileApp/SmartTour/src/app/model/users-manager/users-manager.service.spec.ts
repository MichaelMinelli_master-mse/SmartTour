/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { UsersManagerService } from './users-manager.service';

describe('Service: Login', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UsersManagerService]
    });
  });

  it('should ...', inject([UsersManagerService], (service: UsersManagerService) => {
    expect(service).toBeTruthy();
  }));
});
