import { SessionManagerService } from './../session-manager/session-manager.service';
import { SettingsService } from './../../core/settings/settings.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { BackendProviderService, BackendResponse } from './../backend-provider/backend-provider.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { User } from '../User';

@Injectable()
export class UsersManagerService {
    allUsers: Array<User> = [];

    constructor(private http: HttpClient, private backend: BackendProviderService, private sessionManager: SessionManagerService) {}

    getAll(): Observable<[User]> {
        return Observable.create(observer => {
            this.backend.execute(this.http.get<BackendResponse>(this.backend.LIST_USERS)).subscribe(data => {
                if (data.result) {
                    observer.next(data.data.users.map(user => User.fromJSON(user)));
                } else {
                    observer.error(data);
                }
                observer.complete();
            });
        });
    }

    add(user: User): Observable<BackendResponse> {
        return Observable.create(observer => {
            let fd = new FormData();
            fd.append('firstName', user.firstName);
            fd.append('lastName', user.lastName);
            fd.append('mail', user.mail);
            fd.append('admin', user.placeAdmin ? 'true' : 'false');
            fd.append('password', user.password);

            this.backend.execute(this.http.post<BackendResponse>(this.backend.ADD_USER, fd)).subscribe(data => {
                observer.next(data);
                observer.complete();
            });
        });
    }

    modify(user: User, oldMail: string): Observable<BackendResponse> {
        return Observable.create(observer => {
            let fd = new FormData();
            fd.append('firstName', user.firstName);
            fd.append('lastName', user.lastName);
            fd.append('mail', user.mail);
            fd.append('admin', user.placeAdmin ? 'true' : 'false');

            this.backend.execute(this.http.put<BackendResponse>(this.backend.MODIFY_USER.replace('{mail}', oldMail), fd)).subscribe(data => {
                if (data.result) {
                    if (this.sessionManager.getUser().mail == oldMail) {
                        this.sessionManager.setUser(user);
                    }
                }

                observer.next(data);
                observer.complete();
            });
        });
    }

    password(user: User): Observable<BackendResponse> {
        return Observable.create(observer => {
            let fd = new FormData();
            fd.append('password', user.password);

            this.backend.execute(this.http.put<BackendResponse>(this.backend.MODIFY_PASSWORD.replace('{mail}', user.mail), fd)).subscribe(data => {
                observer.next(data);
                observer.complete();
            });
        });
    }

    delete(user: User): Observable<BackendResponse> {
        return Observable.create(observer => {
            this.backend.execute(this.http.delete<BackendResponse>(this.backend.DELETE_USER.replace('{mail}', user.mail))).subscribe(data => {
                observer.next(data);
                observer.complete();
            });
        });
    }
}
