/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { PlacesManagerService } from './places-manager.service';

describe('Service: Login', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PlacesManagerService]
    });
  });

  it('should ...', inject([PlacesManagerService], (service: PlacesManagerService) => {
    expect(service).toBeTruthy();
  }));
});
