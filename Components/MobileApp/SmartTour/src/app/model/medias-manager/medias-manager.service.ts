import { Place } from './../Place';
import { PointOfInterest } from './../PointOfInterest';
import { SessionManagerService } from './../session-manager/session-manager.service';
import { SettingsService } from './../../core/settings/settings.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { BackendProviderService, BackendResponse } from './../backend-provider/backend-provider.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { User } from '../User';
import { Media } from '../Media';

@Injectable()
export class MediasManagerService {
    allMedias: Array<Media> = [];

    constructor(private http: HttpClient, private backend: BackendProviderService, private sessionManager: SessionManagerService) {}

    getAll(poi: PointOfInterest): Observable<[Media]> {
        return Observable.create(observer => {
            this.backend.execute(this.http.get<BackendResponse>(this.backend.LIST_MEDIAS + '?poi=' + poi.id)).subscribe(data => {
                if (data.result) {
                    observer.next(data.data.medias.map(media => Media.fromJSON(media)));
                } else {
                    observer.error(data);
                }
                observer.complete();
            });
        });
    }

    add(media: Media): Observable<BackendResponse> {
        return Observable.create(observer => {
            let fd = new FormData();
            fd.append('name', media.name);
            fd.append('type', String(media.type));
            fd.append('text', media.text);
            fd.append('condition', JSON.stringify(media.condition));
            fd.append('poi', String(media.poiID));
            fd.append('file', media.file);

            this.backend.execute(this.http.post<BackendResponse>(this.backend.ADD_MEDIA, fd)).subscribe(data => {
                observer.next(data);
                observer.complete();
            });
        });
    }

    modify(media: Media): Observable<BackendResponse> {
        return Observable.create(observer => {
            let fd = new FormData();
            fd.append('name', media.name);
            fd.append('type', String(media.type));
            fd.append('text', media.text);
            fd.append('condition', JSON.stringify(media.condition));
            fd.append('poi', String(media.poiID));
            fd.append('file', media.file);

            this.backend.execute(this.http.put<BackendResponse>(this.backend.MODIFY_MEDIA.replace('{id}', String(media.id)), fd)).subscribe(data => {
                observer.next(data);
                observer.complete();
            });
        });
    }

    delete(media: Media): Observable<BackendResponse> {
        return Observable.create(observer => {
            this.backend.execute(this.http.delete<BackendResponse>(this.backend.DELETE_MEDIA.replace('{id}', String(media.id)))).subscribe(data => {
                observer.next(data);
                observer.complete();
            });
        });
    }
}
