import { ToolboxService } from './../core/toolbox/toolbox.service';
import { BackendProviderService } from './backend-provider/backend-provider.service';
import { User } from './User';

export class Place {
	public FILES_PLACES_IMAGES: string = BackendProviderService.BACKEND_BASE_URL + "places/images/"

	public imgRandomParam: string = ""
	public picture: File = null

	constructor();
	constructor(id: number, name: string, description: string, imageExtension: string);
	constructor(id: number, name: string, description: string, imageExtension: string, users: Array<User>);
	constructor(public id=-1, public name="", public description="", public imageExtension="", public users=Array<User>()) {
		this.imgRandomParam = ToolboxService.IMG_randomParam()
	 }

	static fromJSON(place: any): Place {
		let users = place.users == undefined ? [] : place.users.map(user => User.fromJSON(user))
		return new Place(place.id, place.name, place.description, place.imageExtension, users)
	}

	public refreshImgRandomParam() {
		this.imgRandomParam = ToolboxService.IMG_randomParam()
	}

	public get imagePath(): string {
		return this.FILES_PLACES_IMAGES + String(this.id) + "." + this.imageExtension + "?" + this.imgRandomParam
	}
}
