import { SettingsService } from './../../core/settings/settings.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { BackendProviderService, BackendResponse } from './../backend-provider/backend-provider.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { User } from '../User';

@Injectable()
export class SessionManagerService {
    private user: User = new User();
    public observableUser = new BehaviorSubject<User>(this.user);

    constructor(private http: HttpClient, private backend: BackendProviderService, private settings: SettingsService) {}

    getUser(): User {
        return this.user;
    }

    setUser(user: User) {
        this.user = user;
        this.observableUser.next(this.user);
    }

    tryToLogin(mail: string, password: string): Observable<string> {
        return Observable.create(observer => {
            this.backend.execute(this.http.get<BackendResponse>(this.backend.LOGIN_URL.replace('{mail}', mail).replace('{password}', password))).subscribe(data => {
                if (data.result) {
                    this.setUser(User.fromJSON(data.data.user));

                    observer.next(data);
                } else {
                    switch (data.code) {
                        case -1:
                            data.description = 'login.ERROR_PARAMS';
                            break;

                        case -2:
                            data.description = 'login.ERROR_NO_USER';
                            break;

                        case -3:
                            data.description = 'login.ERROR_PASSWORD';
                            break;

                        default:
                            break;
                    }

                    observer.error(data);
                }
                observer.complete();
            });
        });
    }

    logout(): Observable<void> {
        return Observable.create(observer => {
            this.backend.execute(this.http.get<BackendResponse>(this.backend.LOGOUT_URL)).subscribe(data => {
                this.setUser(new User());

                observer.next();
                observer.complete();
            });
        });
    }

    verifySessionFromPages() {
        this.verifySession(true).subscribe();
    }

    verifySession(withRedirection: boolean): Observable<boolean> {
        return Observable.create(observer => {
            if (this.user.mail == '') {
                this.backend.execute(this.http.get<BackendResponse>(this.backend.RESTORE_SESSION), withRedirection).subscribe(data => {
                    if (data.result) {
                        this.setUser(User.fromJSON(data.data.user));
                    }

                    observer.next(data.result);
                    observer.complete();
                });
            } else {
                observer.next(true);
                observer.complete();
            }
        });
    }
}
