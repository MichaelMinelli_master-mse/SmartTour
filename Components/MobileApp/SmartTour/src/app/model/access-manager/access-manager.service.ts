import { Access } from './../Access';
import { SessionManagerService } from './../session-manager/session-manager.service';
import { SettingsService } from './../../core/settings/settings.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { BackendProviderService, BackendResponse } from './../backend-provider/backend-provider.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { User } from '../User';

@Injectable()
export class AccessManagerService {

	constructor(private http: HttpClient, private backend: BackendProviderService, private sessionManager: SessionManagerService) {	}

	add(access: Access): Observable<BackendResponse> {
		return Observable.create(observer => {
			let fd = new FormData();
			fd.append('mail', access.user.mail);

			this.backend.execute(this.http.post<BackendResponse>(this.backend.ADD_ACCESS.replace("{id}", access.place.id.toString()), fd)).subscribe(
				data => {
					observer.next(data)
					observer.complete()
				}
			)
		});
	}

	delete(access: Access): Observable<BackendResponse> {
		return Observable.create(observer => {
			this.backend.execute(this.http.delete<BackendResponse>(this.backend.DELETE_ACCESS.replace("{id}", access.place.id.toString()).replace("{mail}", access.user.mail))).subscribe(
				data => {
					observer.next(data)
					observer.complete()
				}
			)
		});
	}
}
