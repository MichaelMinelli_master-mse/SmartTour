import { Media } from './../Media';
import { Injectable } from '@angular/core';
import { Content, Refresher } from 'ionic-angular';
import { SettingsService } from '../../core/settings/settings.service';

@Injectable()
export class MediaConditionService {
    constructor(private settings: SettingsService) {}

    private assertAge(ageCondition: any): boolean {
        if (ageCondition == 'all') {
            return true;
        } else {
            let age = this.settings.getAge();

            if (ageCondition.min && age < ageCondition.min)
                    return false

            if (ageCondition.max && age > ageCondition.max)
                return false

            return true;
        }
    }

    showable(media: Media): boolean {
        for (const key of Object.keys(media.condition)) {
            switch (key) {
                case 'age':
                    if (!this.assertAge(media.condition[key])) {
                        return false;
                    }
                    break;

                default:
                    break;
            }
        }

        return true;
    }
}
