import { ToolboxService } from './../core/toolbox/toolbox.service';
import { BackendProviderService } from './backend-provider/backend-provider.service';
import { User } from './User';
import { Place } from './Place';

export enum MediaType {
    TEXT = 1,
    PICTURE = 2,
    VIDEO = 3,
    AUDIO = 4
}

export class Media {
    public file: File = null;

    constructor();
    constructor(id: number);
    constructor(id: number, name: string, type: number, extension: string, text: string, condition: any, poiID: number);
    constructor(public id = -1, public name = '', public type = 0, public extension = '', public text = '', public condition = {}, public poiID = -1) {}

    //Type : 1->Texte, 2->Photo, 3->Video, 4->Audio

    static fromJSON(media: any): Media {
        return new Media(media.id, media.name, media.type, media.extension, media.text, JSON.parse(media.condition), media.poiID);
    }

    get typePictogram(): string {
        switch (this.type) {
            case MediaType.TEXT:
                return 'fa-text-width';
            case MediaType.PICTURE:
                return 'fa-picture-o';
            case MediaType.VIDEO:
                return 'fa-video-camera';
            case MediaType.AUDIO:
                return 'fa-music';
            default:
                return 'fa-exclamation';
        }
    }

    get isText(): boolean {
        return this.type == MediaType.TEXT;
    }

    get downloadURL(): string {
        return BackendProviderService.BACKEND_BASE_URL + 'medias/' + this.id + '.' + this.extension;
    }
}
