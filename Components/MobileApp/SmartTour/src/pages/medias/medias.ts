import { MediaConditionService } from './../../app/model/media-condition/media-condition.service';
import { StreamingMedia, StreamingVideoOptions, StreamingAudioOptions } from '@ionic-native/streaming-media';
import { Media, MediaType } from './../../app/model/Media';
import { MediasManagerService } from './../../app/model/medias-manager/medias-manager.service';
import { PointOfInterest } from './../../app/model/PointOfInterest';
import { ToolboxService } from './../../app/core/toolbox/toolbox.service';
import { TapticEngine } from '@ionic-native/taptic-engine';
import { Component, ViewChild } from '@angular/core';
import { Content, NavController, Refresher, NavParams } from 'ionic-angular';

@Component({
    selector: 'page-medias',
    templateUrl: 'medias.html'
})
export class MediasPage {
    @ViewChild(Content) content: Content;
    @ViewChild(Refresher) refresher: Refresher;

    public poi: PointOfInterest = new PointOfInterest();

    public MediaType = MediaType;
    public medias: any = {};

    constructor(public navCtrl: NavController, public navParams: NavParams, private mediaCondition: MediaConditionService, private mediasManager: MediasManagerService, private toolbox: ToolboxService, public taptic: TapticEngine, private streamingMedia: StreamingMedia) {
        this.poi = navParams.get('poi');
        this.reinitMedias();
    }

    reinitMedias() {
        for (const mediaType in MediaType) {
            if (Number(mediaType)) {
                this.medias[mediaType] = [];
            }
        }
    }

    ionViewDidEnter() {
        this.toolbox.pullToRefresh(this.refresher, this.content);
    }

    play(media: Media) {
        if (media.type == MediaType.AUDIO.valueOf()) {
            let options: StreamingAudioOptions = {
                bgColor: 'white',
                bgImage: 'assets/imgs/logo.png',
                initFullscreen: true
            };
            this.streamingMedia.playAudio(media.downloadURL, options);
        } else {
            this.streamingMedia.playVideo(media.downloadURL);
        }
    }

    doRefresh() {
        this.taptic.impact({ style: 'success' });

        this.mediasManager.getAll(this.poi).subscribe(medias => {
            this.reinitMedias();

            medias.forEach(media => {
                if (this.mediaCondition.showable(media)) {
                    console.log('lédskfhsaédlfkasdélfkj');
                    this.medias[media.type].push(media);
                }
            });

            this.refresher.complete();
        });
    }
}
