import { SettingsService } from './../../app/core/settings/settings.service';
import { PlacesManagerService } from './../../app/model/places-manager/places-manager.service';
import { Place } from './../../app/model/Place';
import { TapticEngine } from '@ionic-native/taptic-engine';
import { Component, ViewChild } from '@angular/core';
import { Content, NavController, Refresher, Searchbar, AlertController } from 'ionic-angular';
import { PointsOfInterestPage } from '../points-of-interest/points-of-interest';
import { SettingsPage } from '../settings/settings';
import { ToolboxService } from '../../app/core/toolbox/toolbox.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'page-smart-tour',
    templateUrl: 'smart-tour.html'
})
export class SmartTourPage {
    @ViewChild(Content) content: Content;
    @ViewChild(Refresher) refresher: Refresher;
    @ViewChild(Searchbar) searchBar: Searchbar;

    private allPlaces: Array<Place> = [];
    public places: Array<Place> = [];

    constructor(public navCtrl: NavController, private translate: TranslateService, private alertCtrl: AlertController, private settings: SettingsService, private placesManager: PlacesManagerService, private toolbox: ToolboxService, private taptic: TapticEngine) {}

    askAge() {
        if (!this.settings.getAge()) {
            let alert = this.alertCtrl.create();
            alert.setTitle(this.translate.instant('settings.age.ASK'));

            alert.addInput({
                type: 'number',
                label: 'Blue'
            });

            //alert.addButton('Cancel');
            alert.addButton({
                text: 'Ok',
                handler: (age: any) => {
                    if (age[0] != '') {
                        this.settings.setAge(age[0]);
                    }

                    this.askAge();
                }
            });

            alert.present();
        }
    }

    ionViewDidEnter() {
        this.askAge();

        this.toolbox.pullToRefresh(this.refresher, this.content);
    }

    goToPointsOfInterest(place: Place) {
        this.navCtrl.push(PointsOfInterestPage, { place: place });
    }

    goToSettings() {
        this.navCtrl.push(SettingsPage);
    }

    search() {
        let searchText = this.searchBar.value.toLowerCase();

        this.places = this.allPlaces.filter(value => {
            return searchText == '' ? true : value.name.toLowerCase().includes(searchText) || value.description.toLowerCase().includes(searchText);
        });
    }

    doRefresh() {
        this.taptic.impact({ style: 'success' });

        this.placesManager.getAll('true').subscribe(places => {
            places = places.sort((a, b) => (a.name < b.name ? -1 : 1));
            this.allPlaces = this.places = places;
            this.search();
            this.refresher.complete();
        });
    }
}
