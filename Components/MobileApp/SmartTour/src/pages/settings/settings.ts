import { TranslateService } from '@ngx-translate/core';
import { TapticEngine } from '@ionic-native/taptic-engine';
import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { SettingsService } from '../../app/core/settings/settings.service';

@Component({
    selector: 'page-settings',
    templateUrl: 'settings.html'
})
export class SettingsPage {
    public age: number = 50;

    constructor(public navCtrl: NavController, public settings: SettingsService, public platform: Platform, public taptic: TapticEngine) {
        let age = settings.getAge();

        if (age) {
            this.age = age;
        }
    }

    ageChange() {
        if (this.platform.is('ios')) {
            this.taptic.selection();
        }

        this.settings.setAge(this.age);
    }
}
