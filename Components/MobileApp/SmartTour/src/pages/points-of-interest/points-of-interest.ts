import { POIsManagerService } from './../../app/model/pois-manager/pois-manager.service';
import { Place } from './../../app/model/Place';
import { ToolboxService } from './../../app/core/toolbox/toolbox.service';
import { SettingsPage } from './../settings/settings';
import { TapticEngine } from '@ionic-native/taptic-engine';
import { Component, ViewChild } from '@angular/core';
import { Content, NavController, Refresher, NavParams, Searchbar } from 'ionic-angular';
import { MediasPage } from '../medias/medias';
import { TranslateService } from '@ngx-translate/core';
import { PointOfInterest } from '../../app/model/PointOfInterest';

@Component({
    selector: 'page-points-of-interest',
    templateUrl: 'points-of-interest.html'
})
export class PointsOfInterestPage {
    @ViewChild(Content) content: Content;
    @ViewChild(Refresher) refresher: Refresher;
    @ViewChild(Searchbar) searchBar: Searchbar;

    public place: Place = new Place();

    private allPOIS: Array<PointOfInterest> = [];
    public pois: Array<PointOfInterest> = [];

    constructor(public navCtrl: NavController, public navParams: NavParams, private poisManager: POIsManagerService, private toolbox: ToolboxService, private taptic: TapticEngine) {
        this.place = navParams.get('place');
    }

    ionViewDidEnter() {
        this.content.resize();
        this.toolbox.pullToRefresh(this.refresher, this.content);
    }

    goToMedias(poi: PointOfInterest) {
        poi.place = this.place;
        this.navCtrl.push(MediasPage, { poi: poi });
    }

    goToSettings() {
        this.navCtrl.push(SettingsPage);
    }

    search() {
        let searchText = this.searchBar.value.toLowerCase();

        this.pois = this.allPOIS.filter(value => {
            return searchText == '' ? true : value.name.toLowerCase().includes(searchText);
        });
    }

    doRefresh() {
        this.taptic.impact({ style: 'success' });

        this.poisManager.getAll(this.place).subscribe(pois => {
            pois = pois.sort((a, b) => (a.order < b.order ? -1 : 1));
            this.allPOIS = this.pois = pois;
            this.search();
            this.refresher.complete();
        });
    }
}
