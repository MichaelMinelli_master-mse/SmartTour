# SmartTour 
## An Advanced Project (PA) from Michaël Minelli


#### Documentation (french)
See the PDF in the "Documentation" folder.



#### Component's technologies
- Mobile Application : [Ionic Framework](https://ionicframework.com)
- Admin Panel : [Angular](https://angular.io)
- Backend API : [Perfect Framework](http://perfect.org)
