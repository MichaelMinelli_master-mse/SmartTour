-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 192.168.0.20
-- Généré le :  jeu. 07 juin 2018 à 14:48
-- Version du serveur :  5.5.55-0+deb7u1-log
-- Version de PHP :  5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `hesso_pa_smarttour`
--

-- --------------------------------------------------------

--
-- Structure de la table `Access`
--

CREATE TABLE `Access` (
  `accessUser` varchar(128) COLLATE utf8_bin NOT NULL,
  `accessPlace` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `Media`
--

CREATE TABLE `Media` (
  `mediaID` int(11) NOT NULL,
  `mediaName` varchar(128) COLLATE utf8_bin NOT NULL,
  `mediaType` int(11) NOT NULL,
  `mediaExtension` varchar(8) COLLATE utf8_bin DEFAULT NULL,
  `mediaText` text COLLATE utf8_bin,
  `mediaCondition` text COLLATE utf8_bin NOT NULL,
  `mediaPOI` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `Place`
--

CREATE TABLE `Place` (
  `placeID` int(11) NOT NULL,
  `placeName` varchar(256) COLLATE utf8_bin NOT NULL,
  `placeDescription` text COLLATE utf8_bin NOT NULL,
  `placeImageExtension` varchar(10) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `PointOfInterest`
--

CREATE TABLE `PointOfInterest` (
  `poiID` int(11) NOT NULL,
  `poiName` varchar(128) COLLATE utf8_bin NOT NULL,
  `poiOrder` int(11) NOT NULL,
  `poiLatitude` double DEFAULT NULL,
  `poiLongitude` double DEFAULT NULL,
  `poiPlace` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `User`
--

CREATE TABLE `User` (
  `userMail` varchar(128) COLLATE utf8_bin NOT NULL,
  `userFirstName` varchar(64) COLLATE utf8_bin NOT NULL,
  `userLastName` varchar(64) COLLATE utf8_bin NOT NULL,
  `userPassword` varchar(128) COLLATE utf8_bin NOT NULL,
  `userPasswordSalt` varchar(32) COLLATE utf8_bin NOT NULL,
  `userPlaceAdmin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `Access`
--
ALTER TABLE `Access`
  ADD PRIMARY KEY (`accessUser`,`accessPlace`),
  ADD KEY `FORKEY_ACCESS_PLACE` (`accessPlace`);

--
-- Index pour la table `Media`
--
ALTER TABLE `Media`
  ADD PRIMARY KEY (`mediaID`),
  ADD KEY `mediaPOI` (`mediaPOI`);

--
-- Index pour la table `Place`
--
ALTER TABLE `Place`
  ADD PRIMARY KEY (`placeID`);

--
-- Index pour la table `PointOfInterest`
--
ALTER TABLE `PointOfInterest`
  ADD PRIMARY KEY (`poiID`),
  ADD KEY `poiPlace` (`poiPlace`);

--
-- Index pour la table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`userMail`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `Media`
--
ALTER TABLE `Media`
  MODIFY `mediaID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `Place`
--
ALTER TABLE `Place`
  MODIFY `placeID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `PointOfInterest`
--
ALTER TABLE `PointOfInterest`
  MODIFY `poiID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `Access`
--
ALTER TABLE `Access`
  ADD CONSTRAINT `FORKEY_ACCESS_PLACE` FOREIGN KEY (`accessPlace`) REFERENCES `Place` (`placeID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FORKEY_ACCESS_USER` FOREIGN KEY (`accessUser`) REFERENCES `User` (`userMail`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `Media`
--
ALTER TABLE `Media`
  ADD CONSTRAINT `Media_ibfk_1` FOREIGN KEY (`mediaPOI`) REFERENCES `PointOfInterest` (`poiID`);

--
-- Contraintes pour la table `PointOfInterest`
--
ALTER TABLE `PointOfInterest`
  ADD CONSTRAINT `PointOfInterest_ibfk_1` FOREIGN KEY (`poiPlace`) REFERENCES `Place` (`placeID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
